package arquitetura.enums

public enum SpringSecurityConfigAttributeOptionsEnum {
    permitAll('permitAll'), isAuthenticated('isAuthenticated()')

    String label

    SpringSecurityConfigAttributeOptionsEnum(String label) {
        this.label = label
    }
}