package arquitetura.enums

import org.springframework.context.MessageSourceResolvable

enum TipoBugEnum implements MessageSourceResolvable {
    BUG('B', 'bug.tipo.bug.label'), MELHORIA('M', 'bug.tipo.melhoria.label')

    String id
    String label

    TipoBugEnum(String id, String label) {
        this.id = id
        this.label = label
    }

    String getKey() {
        return label
    }

    TipoBugEnum getById(String id) {
        return values().find {it.id == id}
    }

    String[] getCodes() {
        return [label]
    }

    Object[] getArguments() {
        return []
    }

    String getDefaultMessage() {
        return label
    }
}