package arquitetura.converters

import arquitetura.enums.TipoBugEnum
import org.grails.databinding.converters.ValueConverter

class TipoBugValueConverter implements ValueConverter {
    boolean canConvert(Object value) {
        value instanceof String && TipoBugEnum.values().id.contains(value)
    }

    Object convert(Object value) {
        return TipoBugEnum.values().find {it.id == value}
    }

    Class<?> getTargetType() {
        return TipoBugEnum
    }
}
