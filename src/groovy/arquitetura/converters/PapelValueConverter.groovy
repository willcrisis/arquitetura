package arquitetura.converters

import arquitetura.seguranca.Papel
import org.apache.commons.lang.StringUtils
import org.grails.databinding.converters.ValueConverter
import utils.TratadorDeParametros

class PapelValueConverter implements ValueConverter {
    boolean canConvert(Object value) {
        value instanceof String && StringUtils.isNumeric(value)
    }

    Object convert(Object value) {
        Papel papel = new Papel()
        papel.id = TratadorDeParametros.somenteNumerosLong(value)
        return papel
    }

    Class<?> getTargetType() {
        return Papel
    }
}
