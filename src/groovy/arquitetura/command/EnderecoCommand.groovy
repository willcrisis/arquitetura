package arquitetura.command

import grails.validation.Validateable

@Validateable
class EnderecoCommand {

    Long id
    Long versao
    String url
    List<String> configAttribute

    static constraints = {
        id nullable: true
        versao nullable: true
        url nullable: false, blank: false
        configAttribute nullable: false, minSize: 1
    }
}
