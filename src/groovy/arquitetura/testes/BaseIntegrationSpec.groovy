package arquitetura.testes

import arquitetura.seguranca.Usuario
import base.BaseController
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.spock.IntegrationSpec
import spock.util.mop.ConfineMetaClassChanges

import java.lang.reflect.ParameterizedType

@ConfineMetaClassChanges([SpringSecurityService])
class BaseIntegrationSpec<T extends BaseController> extends IntegrationSpec {
    protected T instanciaControlador;
    protected Class<T> classeControlador;

    SpringSecurityService springSecurityService

    def setup() {
        classeControlador = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments().first();
        springSecurityService.metaClass.getCurrentUser = { -> Usuario.findByUsername('admin')}
    }

    protected T getControlador() {
        if(!instanciaControlador){
            instanciaControlador = classeControlador.newInstance();
        }

        return instanciaControlador;
    }

    protected T reiniciarControlador() {
        instanciaControlador = classeControlador.newInstance()
        return instanciaControlador
    }

    protected void addParametro(def chave, def valor){
        controlador.params.put(chave, valor);
    }

    protected void addParametros(Map params){
        params.each { addParametro(it.key, it.value); }
    }

    protected void removerParametro(def chave){
        if(!controlador.params.containsKey(chave)){
            return;
        }

        controlador.params.remove(chave)
    }

    protected void removerParametros(List params){
        params.each{ removerParametro(it) };
    }

    protected void setParametro(chave, valor){
        if(!controlador.params.containsKey(chave)){
            throw new IllegalArgumentException("Chave ${chave} não encontrada.");
        }

        controlador.params[chave] = valor;
    }

    protected getParametro(chave){
        if(!controlador.params.containsKey(chave)){
            throw new IllegalArgumentException("Chave ${chave} não encontrada.");
        }

        return controlador.params[chave]
    }

    protected void limparParametros(){
        controlador.params.clear();
    }

    protected void verificarSeErroEhNullable(String code) {
        'nullable' == code
    }

    protected void verificarSeErroEhBlank(String code) {
        'blank' == code
    }

    protected void verificarSeErroEhMaxSize(String code) {
        'maxSize.exceeded' == code
    }
}
