package arquitetura.testes

import arquitetura.seguranca.Usuario
import spock.lang.Specification

import java.lang.reflect.ParameterizedType

abstract class BaseSpecification<T extends Object> extends Specification {
    Class<T> domainClass
    def springSecurityServiceMock = [currentUser: new Usuario(id: 1)]

    def setup() {
        domainClass = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected def getNovaInstancia() {
        def instance = domainClass.newInstance()
        instance.springSecurityService = springSecurityServiceMock
        return instance
    }
}