package arquitetura.seguranca

trait Rastreavel {
    transient springSecurityService

    Date dateCreated
    Date lastUpdated

    Usuario criadoPor
    Usuario atualizadoPor

    static transients = ['springSecurityService']

    def beforeValidate() {
        if (springSecurityService.currentUser) {
            if (!criadoPor) {
                criadoPor = springSecurityService.currentUser
            }
            atualizadoPor = springSecurityService.currentUser
        }
    }
}
