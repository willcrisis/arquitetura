package arquitetura.seguranca

import grails.plugin.springsecurity.userdetails.GrailsUser
import org.springframework.security.core.GrantedAuthority

/**
 * Created with IntelliJ IDEA.
 * User: willian.krause
 * Date: 29/05/14
 * Time: 15:06
 * To change this template use File | Settings | File Templates.
 */
class UsuarioUserDetails extends GrailsUser {

    String nomeCompleto
    String email

    UsuarioUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<GrantedAuthority> authorities, Object id, String nomeCompleto, String email) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities, id)
        this.nomeCompleto = nomeCompleto
        this.email = email
    }
}
