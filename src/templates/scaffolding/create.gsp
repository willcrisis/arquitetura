<%=packageName%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" scope="request" />
		<g:set var="entityNamePlural" value="\${message(code: '${domainClass.propertyName}.plural.label', default: '${className}')}" scope="request" />
		<g:set var="instancia" value="\${${propertyName}}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:render template="form"/>
	</body>
</html>