<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" scope="request" />
	<g:set var="entityNamePlural" value="\${message(code: '${domainClass.propertyName}.plural.label', default: '${className}s')}" scope="request" />
	<g:set var="instancia" value="\${${propertyName}}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	<%  excludedProps = Event.allEvents.toList() << 'id' << 'version' << 'dateCreated' << 'lastUpdated'
	allowedNames = domainClass.persistentProperties*.name
	props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
	Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
	props.each { p -> %>
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				<%  if (p.isEnum()) { %>
				<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
				<%  } else if (p.oneToMany || p.manyToMany) { %>
				<g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
					<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link><br />
				</g:each>
				<%  } else if (p.manyToOne || p.oneToOne) { %>
				<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link>
				<%  } else if (p.type == Boolean || p.type == boolean) { %>
				<g:formatBoolean boolean="\${${propertyName}?.${p.name}}" />
				<%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
				<g:formatDate date="\${${propertyName}?.${p.name}}" />
				<%  } else if (!p.type.isArray()) { %>
				<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
				<%  } %>
			</p>
		</div>
	</div>
	<%  } %>
</body>
</html>