import arquitetura.converters.PapelValueConverter
import arquitetura.converters.TipoBugValueConverter
import arquitetura.seguranca.UsuarioUserDetailsService
import grails.util.Environment
import org.springframework.cache.ehcache.EhCacheFactoryBean
import utils.DatabaseMessageSource

class ArquiteturaGrailsPlugin {
    def version = "0.1.4"
    def grailsVersion = "2.4 > *"
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    def title = "Arquitetura Plugin"
    def author = "Willian Krause"
    def authorEmail = "krause.willian@gmail.com"
    def description = '''\
Plugin que fornece integração com vários frameworks para facilitar a implementação.
'''
    def organization = [ name: "Willcrisis.com", url: "http://www.willcrisis.com/" ]
    def documentation = "http://grails.org/plugin/arquitetura"

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {
        userDetailsService(UsuarioUserDetailsService)
        papelConverter(PapelValueConverter)
        tipoBugConverter(TipoBugValueConverter)
        messageCache(EhCacheFactoryBean) {
            timeToLive = 500
            name = "i18n"
            // other cache properties
        }
        messageSource(DatabaseMessageSource) {
            messageCache = messageCache
            messageBundleMessageSource = ref("messageBundleMessageSource")
        }
        messageBundleMessageSource(org.codehaus.groovy.grails.context.support.PluginAwareResourceBundleMessageSource) {
            basenames = "WEB-INF/grails-app/i18n/messages"
        }
        mergeConfig(application)
    }

    protected mergeConfig(application) {
        application.config.merge(loadConfig(application))
    }

    protected loadConfig(application) {
        new ConfigSlurper(Environment.current.name).parse(application.classLoader.loadClass("ArquiteturaDefaultConfig"))
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { ctx ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }

    def onShutdown = { event ->
    }
}
