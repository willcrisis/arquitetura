//= require jquery
//= require flot/jquery.flot
//= require bootstrap
//= require bootstrap-submenu.min
//= require ripples.min
//= require material.min
//= require_tree .
//= require crossSelect
//= require datepicker/bootstrap-datepicker
//= require datepicker/locales/bootstrap-datepicker.pt-BR
//= require fp
//= require oneToManyEditor
//= require jquery.mask.min
//= require jquery.maskMoney.min
//= require bloodhound
//= require bootstrap3-typeahead
//= require fontawesome-iconpicker.min
//= require jquery.bootstrap-growl.min
//= require flot/jquery.flot.tooltip.min
//= require flot/jquery.flot.resize


$(document).ready(function(){
    $.material.init();
    $('.dropdown-submenu > a').submenupicker();
    carregarMascaras()
    $("select[multiple]").crossSelect({listWidth: 200, font: 14, rows: 13});
    $(".iconpicker").iconpicker({inputSearch: true});
    $("form").submit( function(event) {
        removerClones();
    });

    $('textarea[maxlength]').keyup(function(){
        var limit = parseInt($(this).attr('maxlength'));
        var text = $(this).val();
        var chars = text.length;

        if(chars > limit){
            var new_text = text.substr(0, limit);
            $(this).val(new_text);
        }
        $(this).parent().find('span').html(limit - text.length);
    });
});

function exibirMensagem(mensagem, tipo) {
    $.bootstrapGrowl(mensagem, {type: tipo ? tipo : 'info', width: 'auto'});
}

function carregarMascaras() {
    $(".telefone").mask("(00) 0000-0000");
    $(".cep").mask("00000-000");
    $('.valor').maskMoney({thousands:'.', decimal:',', allowNegative: true});
    $(".numero").mask("0#", {maxlength: false});
    $(".input-append.dateTime").datetimepicker({
        pickSeconds: false,
        language: "pt-BR",
    });
    $(".input-append.dateTime .form-control").mask('00/00/0000 00:00');
    $(".input-append.date").datetimepicker({
        pickTime: false,
        language: "pt-BR"
    });
    $(".input-append.date .form-control").mask('00/00/0000');
}

function removerClones() {
    $('[name*="_clone"]').remove();
}

function recarregarIframe(frameId) {
    $('#' + frameId).attr('src', function(i, val) {
        return val;
    });
}
