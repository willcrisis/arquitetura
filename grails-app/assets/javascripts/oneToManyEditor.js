
function adicionar(id){
    var pai = $("#"+id+"_parent");
    var itens = $(pai).find("div[id^='"+id+"']");

    var childCount = itens.length - 1;

    recalcularIndices(itens);

    var clone = $("#"+id+"_clone").clone();
    var htmlId = id+'['+childCount+'].';
    clone.find(":input").each(function(index, input) {
        if ($(input).attr("name") && $(input).attr("id")) {
            $(input).attr("name", $(input).attr("name").replace("_clone", childCount));
            $(input).attr("id", $(input).attr("id").replace("_clone", childCount));
            if ($(input).attr("onclick")) {
                $(input).attr("onclick", $(input).attr("onclick").replace("_clone", childCount))
            }
            if ($(input).attr("onload")) {
                $(input).attr("onload", $(input).attr("onload").replace("_clone", childCount));
            }
        }
    });
    clone.find("div").each(function(index, input) {
        if ($(input).attr("id")) {
            $(input).attr("id", $(input).attr("id").replace("_clone", childCount))
        }
    });

    clone.attr('id', id+childCount);
    $(pai).append(clone);
    clone.show();

    clone.find(":input").each(function(index, input) {
        if ($(input).attr("onload")) {
            $(input).trigger('load')
        }
    });

    carregarMascaras();
}

function adicionarTabela(id, indice, acima){
    var pai = $("#"+id+"_parent");
    var itens = $(pai).find("tr[class!='clone']");

    var childCount = itens.length - 1;

    var novoIndice = indice ? indice : childCount;

    var clone = $("#"+id+"_clone").clone();
    var htmlId = id+'['+ novoIndice +'].';
    clone.find(":input").each(function(index, input) {
        if ($(input).attr("name") && $(input).attr("id")) {
            $(input).attr("name", $(input).attr("name").replace("_clone", novoIndice));
            $(input).attr("id", $(input).attr("id").replace("_clone", novoIndice));
            if ($(input).attr("onclick")) {
                $(input).attr("onclick", $(input).attr("onclick").replace("_clone", novoIndice))
            }
            if ($(input).attr("onload")) {
                $(input).attr("onload", $(input).attr("onload").replace("_clone", novoIndice));
            }
        }
    });
    clone.find("td").each(function(index, input) {
        if ($(input).attr("id")) {
            $(input).attr("id", $(input).attr("id").replace("_clone", novoIndice))
        }
    });

    clone.removeClass('clone');

    clone.attr('id', id+novoIndice);

    if (indice) {
        var linha = $(itens).eq(Number(indice));
        if (acima) {
            linha.before(clone);
        } else {
            linha.after(clone);
        }
    } else {
        $(pai).append(clone);
    }

    clone.show();

    clone.find(":input").each(function(index, input) {
        if ($(input).attr("onload")) {
            $(input).trigger('load')
        }
    });

    recalcularIndices(itens);

    carregarMascaras();
    window[id + "_parentIniciarPaginacao"](window[id + "_parentMax"]);
}

function adicionarAcima(id, indice){
    adicionarTabela(id, indice, true);
}

function adicionarAbaixo(id, indice){
    adicionarTabela(id, indice, false);
}

function recalcularIndices(itens) {
    itens.each(function(index, div) {
        if (index == 0) {
            return;
        }
        $(div).find(":input").each(function(i, input) {
            var name = $(input).attr("name");
            var primeiraParte = name.substring(0, name.indexOf("[") + 1);
            var segundaParte = name.substring(name.indexOf("]"), name.length);
            $(input).attr("name", primeiraParte + (index - 1) + segundaParte);
            $(input).attr("id", primeiraParte + (index - 1) + segundaParte)
        })
    })
}

function remover(id, indice) {
    $("#"+id+indice).remove();
    var itens = $("#"+id+"_parent").find("div[id^='"+id+"']");
    recalcularIndices(itens);
}

function removerTabela(id, indice) {
    var itens = $("#"+id+"_parent").find("tr[class!='clone']");
    itens.eq(indice).remove();
    recalcularIndices(itens);
    carregarMascaras();
    window[id + "_parentIniciarPaginacao"](window[id + "_parentMax"]);
}
