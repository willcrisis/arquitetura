function incluirBugAjax(url, form) {
    $.post(
        url,
        $('#' + form).serialize(),
        function (data) {
            if (data.sucesso) {
                $('#bugReset').click()
            }
            $('#bugMensagemInterna').html(data.mensagem);
            $('#bugMensagem').collapse()
        }
    );
    return false;
}