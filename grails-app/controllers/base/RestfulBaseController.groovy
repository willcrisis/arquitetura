package base

import arquitetura.utils.ResultBean
import grails.rest.RestfulController
import grails.util.GrailsNameUtils
import org.apache.commons.lang.StringUtils
import org.springframework.dao.DataIntegrityViolationException

import java.lang.reflect.ParameterizedType

import static org.springframework.http.HttpStatus.*

abstract class RestfulBaseController<T extends Object> extends RestfulController {

    static responseFormats = ['json', 'xml']

    Class<T> domainClass;
    def propertyName;
    def propertyNameInstance;
    def propertyNameInstanceList;
    def propertyNameInstanceCount;

    BaseService baseService;

    RestfulBaseController() {
        domainClass = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        propertyName = GrailsNameUtils.getPropertyName(domainClass.simpleName)
        propertyNameInstance = propertyName + "Instance"
        propertyNameInstanceList = propertyNameInstance + "List"
        propertyNameInstanceCount = propertyNameInstance + "Count"
    }

    def index() {
        respond internalList()
    }

    protected def internalList() {
        new ResultBean(itens: (domainClass).list(params), count: (domainClass).count())
    }


    def save() {
        def instance = domainClass.newInstance(params)

        if (internalSave(instance)) {
            respond instance, [status: CREATED]
        } else {
            respond instance, [status: INTERNAL_SERVER_ERROR, errors: instance.errors]
        }
    }

    protected def internalSave(def instance) {
        baseService.save(instance)
    }

    def show() {
        def instance = internalShow()
        if (!instance) {
            render status: NOT_FOUND
            return
        }
        respond instance
    }

    protected def internalShow() {
        find()
    }

    def update() {
        def instance = find()
        if (!instance) {
            return
        }

        if (internalUpdate(instance)) {
            respond instance, [status: OK]
        } else {
            respond instance, [status: INTERNAL_SERVER_ERROR, errors: instance.errors]
        }
    }

    protected def internalUpdate(def instance) {
        instance.properties = params
        internalSave(instance)
    }

    def delete() {
        def instance = find()
        if (!instance) {
            return
        }

        try {
            internalDelete(instance)
            render status: NO_CONTENT
        } catch (DataIntegrityViolationException ex) {
            respond instance, [status: INTERNAL_SERVER_ERROR, errors: ex.getMessage()]
        }
    }

    private def internalDelete(def instance) {
        baseService.delete(instance)
    }

    def find() {
        if (!params.id || !StringUtils.isNumeric(params.id)) {
            render status: INTERNAL_SERVER_ERROR
            return null
        }
        def instance = baseService.find(domainClass, params.id)
        if (!instance) {
            render status: NOT_FOUND
            return null
        }
        return instance
    }
}