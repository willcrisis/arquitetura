package base

import arquitetura.utils.ResultBean
import grails.util.GrailsNameUtils
import org.springframework.dao.DataIntegrityViolationException

import java.lang.reflect.ParameterizedType

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.OK

abstract class BaseController<T extends Object> {
    Class<T> domainClass;
    def propertyName;
    def propertyNameInstance;
    def propertyNameInstanceList;
    def propertyNameInstanceCount;

    BaseService baseService;
    def filterPaneService

    BaseController() {
        domainClass = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        propertyName = GrailsNameUtils.getPropertyName(domainClass.simpleName)
        propertyNameInstance = propertyName + "Instance"
        propertyNameInstanceList = propertyNameInstance + "List"
        propertyNameInstanceCount = propertyNameInstance + "Count"
    }

    def index() {
        flash.message = null
        forward action: "list", params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int("max") : 10, 100)
        ResultBean entitiesFound = internalList();
        respond entitiesFound.itens, model: [(propertyNameInstanceCount): entitiesFound.count]
    }

    def create() {
        T instance = params.instance ?: domainClass.newInstance(params)
        respond instance
    }

    def edit() {
        respond find()
    }

    def save() {
        removerClones()

        def instance = domainClass.newInstance(params)

        if (!internalSave(instance)) {
            respond instance.errors, view: "create"
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: "default.created.message", args: [message(code: "${propertyName}.label", default: "${domainClass}"), getAtributoQuandoSalvoOuExcluido(instance)])
                params.controladorRetorno ? redirect(controller: params.controladorRetorno, action: params.actionRetorno ?: "list") : redirect(instance)
            }
            '*' { respond instance, [status: CREATED]}
        }
    }

    protected void removerClones() {
        def clones = params.findAll {
            it.key?.toString()?.contains("[_clone]")
        }

        clones.each {
            params.remove(it.key)
        }
    }

    def update() {
        removerClones()

        def instance = find()

        if (!internalUpdate(instance)) {
            respond instance.errors, view: "edit"
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: "default.updated.message", args: [message(code: "${propertyName}.label", default: "${domainClass}"), getAtributoQuandoSalvoOuExcluido(instance)])
                params.controladorRetorno ? redirect(controller: params.controladorRetorno, action: params.actionRetorno ?: "list") : redirect(instance)
            }
            '*' { respond instance, [status: OK]}
        }


    }

    def internalSave(entity) {
        baseService.save(entity)
    }

    def internalUpdate(entity) {
        entity.properties = params;
        internalSave(entity)
    }

    protected ResultBean internalList() {
        new ResultBean(itens: (domainClass).list(params), count: (domainClass).count())
    }

    def show() {
        def instance = find()
        if (!instance) {
            flash.message = message(code: "default.not.found.message", args: [message(code: "${propertyName}.label", default: "${domainClass}"), params.id])
            redirect action: "list"
            return
        }

        respond instance
    }

    def delete() {
        def instance = find();

        params.remove("_action_delete");

        if (instance) {
            try {
                internalDelete(instance)
                flash.message = message(code: "default.deleted.message", args: [message(code: "${propertyName}.label", default: "${domainClass}"), getAtributoQuandoSalvoOuExcluido(instance)])
                redirect action:"index", method:"GET"
            } catch (DataIntegrityViolationException ex) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: "${propertyName}.label", default: '${className}'),getAtributoQuandoSalvoOuExcluido(instance)])
                redirect action:"show", params: params
                return
            }
        } else {
            flash.message = message(code: 'default.not.found.message', args: [message(code: "${propertyName}.label", default: "${domainClass}"), getAtributoQuandoSalvoOuExcluido(instance)])
            redirect action: "index", method: "GET"
        }
        forward action: "list", params: params
    }

    def internalDelete(def entity) {
        baseService.delete(entity)
    }

    T find() {
        baseService.find(domainClass, params.id)
    }

    def filter() {
        params.max = Math.min(params.max ? params.int("max") : 10, 100)
        def entitiesFound = internalFilter();
        render view: 'list', model: [
                (propertyNameInstanceList): entitiesFound.result,
                (propertyNameInstanceCount): entitiesFound.count,
                filterParams: org.grails.plugin.filterpane.FilterPaneUtils.extractFilterParams(params),
                params: params
        ]
    }

    def internalFilter() {
        return [result: filterPaneService.filter(params, domainClass),
                count: filterPaneService.count(params, domainClass)]
    }

    abstract def getAtributoQuandoSalvoOuExcluido(def instancia)
}
