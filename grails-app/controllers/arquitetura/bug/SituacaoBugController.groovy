package arquitetura.bug

import base.BaseController

class SituacaoBugController extends BaseController<SituacaoBug> {

    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return instancia.label
    }
}
