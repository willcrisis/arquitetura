package arquitetura.bug

import base.BaseController
import grails.converters.JSON
import utils.TratadorDeParametros

class BugController extends BaseController<Bug> {

    def bugService

    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return ''
    }

    def list() {
        if (!params.sort) {
            params.sort = 'idSituacao'
        }
        if (!params.order) {
            params.order = 'asc'
        }
        super.list()
    }

    def ajaxSave() {
        Map result = [:]
        Bug instancia = Bug.newInstance(params)
        if (!internalSave(instancia)) {
            result.sucesso = false
            result.mensagem = message(code: 'bug.cadastro.falha.message')
            render result as JSON
            return
        }
        result.sucesso = true
        result.mensagem = message(code: 'bug.cadastro.sucesso.message')
        sendMail {
            async true
            to grailsApplication.config.grails.bugs.email
            subject message(code: 'bug.cadastro.email.assunto')
            html g.render(template: '/bug/mail/novoBug', model: [bug: instancia, urlServidor: grailsApplication.config.grails.serverURL, sistema: message(code: 'sistema.nome.label')], plugin: 'arquitetura')
        }
        render result as JSON
    }

    def internalSave(def instancia) {
        bugService.incluir(instancia)
    }

    def internalUpdate(def instancia) {
        instancia.properties = params
        bugService.alterar(instancia, TratadorDeParametros.somenteNumerosLong(params.situacaoBug), params.observacoes)
    }
}
