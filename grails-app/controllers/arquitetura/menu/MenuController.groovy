package arquitetura.menu

import base.BaseController
import utils.TratadorDeParametros

class MenuController extends BaseController<Menu> {

    def menuService

    def alternar() {
        Menu resultado = menuService.alternarAtivacao(TratadorDeParametros.somenteNumerosLong(params.id))
        if (!resultado.hasErrors()) {
            flash.message = message(code: 'menu.ativacao.sucesso.message', args: [resultado.label, resultado.ativo ? message(code: 'menu.ativado.label', default: 'ativado') : message(code: 'menu.desativado.label', default: 'desativado')])
        }
        forward(action: 'list')
    }

    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return instancia.label
    }
}
