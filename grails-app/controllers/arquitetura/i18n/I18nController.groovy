package arquitetura.i18n

import base.BaseController
import net.sf.ehcache.Ehcache

class I18nController extends BaseController<I18n> {

    Ehcache messageCache
    def i18nService

    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return instancia.key
    }

    def popup() {
        I18n instancia = i18nService.consultar(params.key, request.locale)
        if (!instancia) {
            instancia = new I18n(params)
        }
        respond instancia
    }

    def internalSave(def instance) {
        messageCache.flush()
        super.internalSave(instance)
    }

    def salvarPopup() {
        I18n instancia = find()
        if (!instancia) {
            instancia = new I18n()
        }
        if (internalUpdate(instancia)) {
            respond instancia, view: '/layouts/texto', model: [mensagem: message(code: "default.saved.message", args: [message(code: "i18n.label"), getAtributoQuandoSalvoOuExcluido(instancia)])]
        } else {
            respond instancia.errors, view: 'popup'
        }
    }
}
