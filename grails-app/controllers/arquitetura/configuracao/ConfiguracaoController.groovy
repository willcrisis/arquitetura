package arquitetura.configuracao

import base.BaseController

class ConfiguracaoController extends BaseController<Configuracao> {
    def getAtributoQuandoSalvoOuExcluido(def instancia) {
        return instancia.chave
    }
}
