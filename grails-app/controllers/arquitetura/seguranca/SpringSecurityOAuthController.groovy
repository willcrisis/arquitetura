package arquitetura.seguranca

import arquitetura.command.UsuarioCommand
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.oauth.OAuthToken
import org.apache.commons.lang.RandomStringUtils
import org.springframework.security.core.context.SecurityContextHolder

class SpringSecurityOAuthController {

    def oauthService
    def grailsApplication
    def usuarioService

    def onSuccess() {
        def token = session[oauthService.findSessionKeyForAccessToken(params.provider)]
        OAuthToken providerToken = createAuthToken(params.provider, token)

        if (providerToken.principal instanceof UsuarioUserDetails) {
            authenticateAndRedirect(providerToken, defaultTargetUrl)
        } else {
            def response = oauthService."get${params.provider.capitalize()}Resource"(token, grailsApplication.config.grails."${params.provider}".api.url)
            def user = JSON.parse(response.body)

            def oAuthID = new OAuthID(provider: params.provider, accessToken: providerToken.socialId)

            Usuario usuario = Usuario.findByUsername(user.email)
            if (usuario) {
                oAuthID.user = usuario
                usuario.addTooAuthIDs(oAuthID)
                usuarioService.save(usuario)
                popularAutenticacao(providerToken, usuario)
                authenticateAndRedirect(providerToken, defaultTargetUrl)
            } else {
                if (params.provider == 'twitter') {
                    session['oAuthID'] = oAuthID
                    forward controller: 'usuario', action: 'registro', params: [email: user.email, nomeCompleto: user.name]
                } else {
                    String senha = RandomStringUtils.random(10)
                    usuarioService.registrar(new UsuarioCommand(email: user.email, nomeCompleto: user.name, novaSenha: senha, confirmacaoSenha: senha), oAuthID)
                    usuario = Usuario.findByUsername(user.email)
                    popularAutenticacao(providerToken, usuario)
                    authenticateAndRedirect(providerToken, defaultTargetUrl)
                }
            }
        }
    }

    protected OAuthToken createAuthToken(providerName, scribeToken) {
        def providerService = grailsApplication.mainContext.getBean("${providerName}SpringSecurityOAuthService")
        OAuthToken oAuthToken = providerService.createAuthToken(scribeToken)

        def oAuthID = OAuthID.findByProviderAndAccessToken(oAuthToken.providerName, oAuthToken.socialId)
        if (oAuthID) {
            popularAutenticacao(oAuthToken, oAuthID.user)
        }

        return oAuthToken
    }

    private void popularAutenticacao(OAuthToken token, Usuario usuario) {
        token.principal = criarPrincipal(usuario)
        token.authorities = usuario.authorities
        token.authenticated = true
    }

    private static UsuarioUserDetails criarPrincipal(Usuario user) {
        return new UsuarioUserDetails(user.username, user.password, user.enabled, !user.accountExpired, !user.passwordExpired, !user.accountExpired, user.authorities, user.id, user.nomeCompleto, user.email)
    }

    protected Map getDefaultTargetUrl() {
        def config = SpringSecurityUtils.securityConfig
        def savedRequest = SpringSecurityUtils.getSavedRequest(session)
        def defaultUrlOnNull = '/'

        if (savedRequest && !config.successHandler.alwaysUseDefault) {
            return [url: (savedRequest.redirectUrl ?: defaultUrlOnNull)]
        } else {
            return [uri: (config.successHandler.defaultTargetUrl ?: defaultUrlOnNull)]
        }
    }

    protected void authenticateAndRedirect(OAuthToken oAuthToken, redirectUrl) {
        SecurityContextHolder.context.authentication = oAuthToken
        redirect(redirectUrl instanceof Map ? redirectUrl : [uri: redirectUrl])
    }
}
