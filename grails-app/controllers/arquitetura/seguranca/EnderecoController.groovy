package arquitetura.seguranca

import arquitetura.command.EnderecoCommand
import arquitetura.enums.SpringSecurityConfigAttributeOptionsEnum
import base.BaseController
import utils.TratadorDeParametros

class EnderecoController extends BaseController<Endereco> {

    def enderecoService

    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return instancia.url
    }

    def create() {
        Map atributos = [enderecoInstance: new EnderecoCommand()]
        return popularPermissoes(atributos)
    }

    def edit() {
        Map atributos = [enderecoInstance: enderecoService.criarCommand(TratadorDeParametros.somenteNumerosLong(params.id))]
        return popularPermissoes(atributos)
    }

    def save(EnderecoCommand command) {
        internalSave(command, 'create', 'default.created.message')
    }

    def update(EnderecoCommand command) {
        internalSave(command, 'edit', 'default.updated.message')
    }

    def list() {
        params.sort = params.sort ?: 'url'
        super.list()
    }

    private def internalSave(EnderecoCommand command, String view, String mensagem) {
        command.id = TratadorDeParametros.somenteNumerosLong(params.id)
        command.versao = TratadorDeParametros.somenteNumerosLong(params.versao)
        enderecoService.salvarEndereco(command)
        if (command.hasErrors()) {
            render view: view, model: [enderecoInstance: command]
            return
        }

        flash.message = message(code: mensagem, args: [message(code: "endereco.label", default: "Endereco"), getAtributoQuandoSalvoOuExcluido(command)])
        redirect(action: "list")
    }

    private Map popularPermissoes(Map atributos) {
        atributos.permissoes = SpringSecurityConfigAttributeOptionsEnum.values().collect {it.label}
        Papel.list().authority.each {
            atributos.permissoes << it
        }
        return atributos
    }
}
