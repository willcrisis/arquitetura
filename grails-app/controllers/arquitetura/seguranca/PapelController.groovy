package arquitetura.seguranca

import arquitetura.seguranca.Papel
import base.BaseController

class PapelController extends BaseController<Papel> {
    def getAtributoQuandoSalvoOuExcluido(def Object instancia) {
        return instancia.authority
    }
}
