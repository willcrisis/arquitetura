package arquitetura.seguranca

import arquitetura.command.UsuarioCommand
import base.BaseController
import utils.TratadorDeParametros

class UsuarioController extends BaseController<Usuario> {

    def usuarioService
    def springSecurityService

    def getAtributoQuandoSalvoOuExcluido(instancia) {
        return instancia.username
    }

    def create() {
        [usuarioInstance: new UsuarioCommand()]
    }

    def edit() {
        prepararCommand()
    }

    def save(UsuarioCommand command) {
        internalSave(command, 'create', 'default.created.message')
    }

    def update(UsuarioCommand command) {
        command.alteracao = true
        internalSave(command, 'edit', 'default.updated.message')
    }

    private def internalSave(UsuarioCommand command, String view, String mensagem) {
        command.id = TratadorDeParametros.somenteNumerosLong(params.id)
        command.versao = TratadorDeParametros.somenteNumerosLong(params.versao)
        usuarioService.salvarUsuario(command)
        if (command.hasErrors()) {
            render view: view, model: [usuarioInstance: command]
            return
        }

        flash.message = message(code: mensagem, args: [message(code: "usuario.label", default: "Usuario"), getAtributoQuandoSalvoOuExcluido(command)])
        redirect(action: "list")
    }

    def show() {
        prepararCommand()
    }

    def alternar() {
        Usuario usuario = usuarioService.alternarCampo(TratadorDeParametros.somenteNumerosLong(params.id), params.campo, params.versao)
        if (usuario.hasErrors()) {
            params.errors = usuario.errors.allErrors
        } else {
            flash.message = message(code: 'usuario.alternar.sucesso.message', args: [message(code: "usuario.${params.campo}.label", default: 'Campo')])
        }
        forward(action: 'list')
    }

    def permissoesPopup() {
        prepararCommand()
    }

    def salvarPermissoes(UsuarioCommand command) {
        Usuario usuario = find()
        usuarioService.salvarPermissoes(command, usuario)
        if (command.hasErrors()) {
            render view: 'permissoesPopup', model: [usuarioInstance: command]
        } else {
            render view: '/layouts/texto', model: [mensagem: message(code: 'usuario.permissoes.sucesso.message')]
        }

    }

    def minhaConta() {
        prepararCommand()
    }

    def alterarSenhaPopup() {
        prepararCommand()
    }

    def alterarSenha(UsuarioCommand command) {
        command.id = TratadorDeParametros.somenteNumerosLong(params.id)
        command.versao = TratadorDeParametros.somenteNumerosLong(params.versao)
        usuarioService.alterarSenha(command)
        if (command.hasErrors()) {
            render view: 'alterarSenhaPopup', model: [usuarioInstance: command]
        } else {
            render view: '/layouts/texto', model: [mensagem: message(code: 'usuario.senhaAlterada.message')]
        }
    }

    def registro() {
        return [usuarioInstance: new UsuarioCommand(email: params.email, nomeCompleto: params.nomeCompleto)]
    }

    def registrar(UsuarioCommand command) {
        usuarioService.registrar(command, session['oAuthID'])
        if (command.hasErrors()) {
            render view: 'registro', model: [usuarioInstance: command]
        } else {
            session.removeAttribute('oAuthID')
            springSecurityService.reauthenticate(command.email)
            redirect(controller: 'login', params: [message: message(code: 'usuario.registradoSucesso.message', args: [command.email])])
        }
    }

    private def prepararCommand() {
        [usuarioInstance: usuarioService.criarCommand(TratadorDeParametros.somenteNumerosLong(params.id))]
    }
}
