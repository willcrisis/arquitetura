// configuration for plugin testing - will not be included in the plugin zip

log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
}

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.oauth.domainClass = 'arquitetura.seguranca.OAuthID'
grails.plugin.springsecurity.userLookup.userDomainClassName = 'arquitetura.seguranca.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'arquitetura.seguranca.Permissao'
grails.plugin.springsecurity.authority.className = 'arquitetura.seguranca.Papel'
grails.plugin.springsecurity.requestMap.className = 'arquitetura.seguranca.Endereco'
grails.plugin.springsecurity.securityConfigType = 'Requestmap'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	'/':                              ['permitAll'],
	'/index':                         ['permitAll'],
	'/index.gsp':                     ['permitAll'],
	'/assets/**':                     ['permitAll'],
	'/**/js/**':                      ['permitAll'],
	'/**/css/**':                     ['permitAll'],
	'/**/images/**':                  ['permitAll'],
	'/**/favicon.ico':                ['permitAll']
]
grails.plugin.springsecurity.logout.postOnly = false

grails.plugin.springsecurity.filterChain.chainMap = [
        '/api/**': 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter',  // Stateless chain
        '/**': 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'                                                                          // Traditional chain
]

grails.plugin.springsecurity.rest.token.storage.useGorm = true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = 'arquitetura.seguranca.AuthenticationToken'

grails.bugs.email = 'krause.willian@gmail.com'

google.analytics.webPropertyID = 'UA-58309057-1'

grails.facebook.api.url = 'https://graph.facebook.com/me?fields=id,name,email,picture'
grails.google.api.url = 'https://www.googleapis.com/oauth2/v1/userinfo'
grails.twitter.api.url = 'https://api.twitter.com/1.1/account/verify_credentials.json'

environments {
    production {
        grails.plugin.databasemigration.updateOnStart = true
        grails.plugin.databasemigration.updateOnStartFileNames = ['changelog.groovy']
        grails.plugin.databasemigration.changelogLocation = 'migrations'
    }
}