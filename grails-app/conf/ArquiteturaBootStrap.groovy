import arquitetura.bug.SituacaoBug
import arquitetura.menu.Menu
import arquitetura.menu.MenuPapel
import arquitetura.seguranca.Endereco
import arquitetura.seguranca.Papel
import arquitetura.seguranca.Permissao
import arquitetura.seguranca.Usuario

class ArquiteturaBootStrap {

    def springSecurityService

    def init = { servletContext ->
        environments {
            development {
                Papel user = new Papel(authority: 'ROLE_USER').save(flush: true, failOnError: true)
                Papel admin = Papel.findByAuthority('ROLE_ADMIN') ?: new Papel(authority: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                Usuario usuarioAdmin = new Usuario(username: 'admin', password: 'admin', nomeCompleto: 'Administrador', email: 'admin@willcrisis.com').save(flush: true, failOnError: true)
                Permissao.create(
                        usuarioAdmin,
                        admin,
                        true
                )
                Permissao.create(
                        usuarioAdmin,
                        user,
                        true
                )
                for (String url in [
                        '/**/favicon.ico', '/assets/**',
                        '/**/js/**', '/**/css/**', '/**/images/**', '/**/fonts/**',
                        '/login', '/login.*', '/login/*', '/oauth/**', '/oauthCallback/**',
                        '/logout', '/logout.*', '/logout/*', '/dbconsole/**']) {
                    new Endereco(url: url, configAttribute: 'permitAll').save(flush: true, failOnError: true)
                }

                for (String url in ['/', '/index', '/index.gsp']) {
                    new Endereco(url: url, configAttribute: 'isAuthenticated()').save(flush: true, failOnError: true)
                }

                for (String url in ['/usuario/registro', '/usuario/registro/**', '/usuario/registrar', '/usuario/registrar/**']) {
                    new Endereco(url: url, configAttribute: 'isAnonymous()').save(flush: true, failOnError: true)
                }

                new Endereco(url: '/menu/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/usuario/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/usuario/minhaConta/**', configAttribute: 'isAuthenticated()').save(flush: true, failOnError: true)
                new Endereco(url: '/usuario/alterarSenhaPopup/**', configAttribute: 'isAuthenticated()').save(flush: true, failOnError: true)
                new Endereco(url: '/usuario/alterarSenha/**', configAttribute: 'isAuthenticated()').save(flush: true, failOnError: true)
                new Endereco(url: '/papel/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/i18n/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/endereco/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/dbconsole/**', configAttribute: 'permitAll').save(flush: true, failOnError: true)
                new Endereco(url: '/situacaoBug/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/bug/create/**', configAttribute: 'isAuthenticated()').save(flush: true, failOnError: true)
                new Endereco(url: '/bug/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)
                new Endereco(url: '/configuracao/**', configAttribute: 'ROLE_ADMIN').save(flush: true, failOnError: true)

                Menu menuConfiguracao = new Menu(label: 'menu.configuracao.label', ehPai: true, ordem: 100, ativo: true, icone: 'fa-gears').save(flush: true, failOnError: true)
                Menu usuario = new Menu(label: 'usuario.plural.label', controlador: 'usuario', acao: 'index', ehPai: false, ordem: 10, ativo: true, pai: menuConfiguracao, icone: 'fa-users').save(flush: true, failOnError: true)
                Menu menuSeguranca = new Menu(label: 'menu.seguranca.label', ehPai: true, ordem: 20, ativo: true, pai: menuConfiguracao, icone: 'fa-lock').save(flush: true, failOnError: true)
                Menu i18n = new Menu(label: 'i18n.plural.label', pai: menuConfiguracao, controlador: 'i18n', acao: 'index', ehPai: false, ordem: 30, ativo: true, icone: 'fa-flag').save(flush: true, failOnError: true)
                Menu papel = new Menu(label: 'papel.plural.label', pai: menuSeguranca, controlador: 'papel', acao: 'index', ehPai: false, ordem: 10, ativo: true, icone: 'fa-pencil-square-o').save(flush: true, failOnError: true)
                Menu menu = new Menu(label: 'menu.plural.label', pai: menuSeguranca, controlador: 'menu', acao: 'index', ehPai: false, ordem: 20, ativo: true, icone: 'fa-sitemap').save(flush: true, failOnError: true)
                Menu endereco = new Menu(label: 'endereco.plural.label', pai: menuSeguranca, controlador: 'endereco', acao: 'index', ehPai: false, ordem: 30, ativo: true, icone: 'fa-envelope').save(flush: true, failOnError: true)
                Menu situacaoBug = new Menu(label: 'situacaoBug.plural.label', pai: menuConfiguracao, controlador: 'situacaoBug', acao: 'index', ehPai: false, ordem: 40, ativo: true, icone: 'fa-bug').save(flush: true, failOnError: true)
                Menu bug = new Menu(label: 'bug.plural.label', pai: menuConfiguracao, controlador: 'bug', acao: 'index', ehPai: false, ordem: 50, ativo: true, icone: 'fa-bug').save(flush: true, failOnError: true)
                Menu configuracao = new Menu(label: 'configuracao.plural.label', pai: menuConfiguracao, controlador: 'configuracao', acao: 'index', ehPai: false, ordem: 5, ativo: true, icone: 'fa-gear').save(flush: true, failOnError: true)

                new MenuPapel(menu: menuConfiguracao, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: menuSeguranca, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: usuario, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: menu, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: papel, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: i18n, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: endereco, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: situacaoBug, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: bug, papel: admin).save(flush: true, failOnError: true)
                new MenuPapel(menu: configuracao, papel: admin).save(flush: true, failOnError: true)

                new SituacaoBug(label: 'situacaoBug.aberto.label').save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.emCorrecao.label').save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.pausado.label').save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.corrigido.label').save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.naoProcede.label').save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.finalizado.label', fechado: true).save(flush: true, failOnError: true)
                new SituacaoBug(label: 'situacaoBug.cancelado.label', fechado: true).save(flush: true, failOnError: true)
            }
        }
    }
    def destroy = {
    }
}