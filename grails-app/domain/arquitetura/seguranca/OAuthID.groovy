package arquitetura.seguranca

/**
 * Created by willian.krause on 23/07/2015.
 */
class OAuthID {
    String provider
    String accessToken

    static belongsTo = [user: Usuario]

    static constraints = {
        accessToken unique: true
    }

    static mapping = {
        provider    index: "identity_idx"
        accessToken index: "identity_idx"
    }
}
