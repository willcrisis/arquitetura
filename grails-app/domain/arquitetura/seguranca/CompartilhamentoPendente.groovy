package arquitetura.seguranca

class CompartilhamentoPendente {
    String email
    Long idRegistro
    Class tipo
    String service

    static constraints = {
        email nullable: false, email: true
        idRegistro nullable: false
        tipo nullable: false
        service nullable: false
    }
}
