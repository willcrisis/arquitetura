package arquitetura.seguranca

import org.springframework.security.core.GrantedAuthority

class Papel implements GrantedAuthority {

    String authority

    static mapping = {
        cache true
    }

    static constraints = {
        authority blank: false, unique: true
    }
}
