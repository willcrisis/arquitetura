package arquitetura.i18n

class I18n {
    String key
    Locale language
    String value

    static mapping = {
        cache true
        columns {
            key index: 'idx_message_key'
            language index: 'idx_message_key'
        }
    }

    static constraints = {
        key unique: 'language', nullable: false, blank: false, maxSize: 200
        language nullable: false
        value nullable: false, maxSize: 200
    }

    String toString() {
        value
    }
}
