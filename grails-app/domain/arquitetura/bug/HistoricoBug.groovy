package arquitetura.bug

class HistoricoBug {
    Date data
    String observacoes

    static belongsTo = [
            bug: Bug,
            situacao: SituacaoBug
    ]

    static mapping = {
        observacoes sqlType: 'longtext'
    }

    static constraints = {
        data nullable: false
        observacoes nullable: true, blank: true, maxSize: 3000
        bug nullable: false
        situacao nullable: false
    }
}
