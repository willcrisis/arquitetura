package arquitetura.bug

import arquitetura.enums.TipoBugEnum
import arquitetura.seguranca.Usuario

class Bug {

    String titulo
    String descricao
    Date data

    TipoBugEnum tipo

    Long idSituacao
    SituacaoBug situacao

    static hasMany = [historico: HistoricoBug]

    static transients = ['situacao']

    static belongsTo = [
            usuario: Usuario
    ]

    static mapping = {
        descricao sqlType: 'longtext'
        idSituacao formula: '(SELECT hb.situacao_id FROM historico_bug hb WHERE hb.bug_id = id ORDER BY hb.data DESC LIMIT 1)'
        historico sort: 'data', order: 'desc'
    }

    static constraints = {
        titulo nullable: false, blank: false, maxSize: 150
        descricao nullable: false, blank: false, maxSize: 3000
        tipo nullable: false
        usuario nullable: false
        data nullable: false
    }

    SituacaoBug getSituacao() {
        if (!situacao && idSituacao) {
            situacao = SituacaoBug.findById(idSituacao)
        }
        return situacao
    }
}
