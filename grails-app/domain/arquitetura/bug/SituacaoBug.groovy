package arquitetura.bug

class SituacaoBug {

    transient mensagemService

    String label
    boolean fechado

    static transients = ['mensagemService']

    static constraints = {
        label nullable: false, blank: false, maxSize: 200
    }

    static namedQueries = {
        emAndamento {
            eq 'label', 'situacaoBug.aberto.label'
            maxResults 1
        }
    }

    String toString() {
        mensagemService.traduzir(label)
    }
}
