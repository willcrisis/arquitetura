package arquitetura.menu

import arquitetura.seguranca.Papel

class Menu {

    def mensagemService

    String label
    String controlador
    String acao
    String icone
    Integer ordem
    boolean ehPai
    boolean ativo

    Menu pai

    static hasMany = [permissoes: Papel]

    static mapping = {
        permissoes joinTable: [name: 'menu_papel', key: 'menu_id']
    }

    static constraints = {
        label nullable: false, blank: false, maxSize: 200
        controlador nullable: true, blank: true, maxSize: 200, validator: validarCondicional
        acao nullable: true, blank: true, maxSize: 200, validator: validarCondicional
        pai nullable: true
        icone nullable: true, blank: true, maxSize: 50
    }

    String getTitulo() {
        mensagemService.traduzir(label)
    }

    @Override
    String toString() {
        return titulo
    }

    static validarCondicional = { def valor, Menu obj ->
        if (!valor && !obj.ehPai) {
            return 'default.null.message'
        }
    }

    def beforeValidate() {
        if (this.ehPai) {
            controlador = null
            acao = null
        }
    }

    def beforeDelete() {
        MenuPapel.removeAll(this)
    }
}
