package arquitetura.menu

import arquitetura.seguranca.Papel

class MenuPapel implements Serializable {
    static belongsTo = [menu: Menu, papel: Papel]

    static mapping = {
        version false
        id composite: ['menu', 'papel']
        papel lazy: false
    }

    static void removeAll(Menu m) {
        MenuPapel.where {
            menu == m
        }.deleteAll()
    }

    static constraints = {
        menu nullable: false
        papel nullable: false
    }
}
