package utils

import groovy.transform.Immutable

@Immutable
class MessageKey implements Serializable {
    String code
    Locale locale
}
