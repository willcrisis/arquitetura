package utils

import arquitetura.i18n.I18n
import net.sf.ehcache.Ehcache
import net.sf.ehcache.Element
import org.springframework.context.support.AbstractMessageSource

import java.text.MessageFormat

class DatabaseMessageSource extends AbstractMessageSource {

    Ehcache messageCache
    def messageBundleMessageSource

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        def key = new MessageKey(code, locale)
        def format = messageCache.get(key)?.value
        if (!format) {
            I18n i18n = I18n.findByKeyAndLanguage(code, locale)

            if (i18n) {
                format = new MessageFormat(i18n.value, i18n.language)
            } else {
                format = messageBundleMessageSource.resolveCode(code, locale)
            }
            messageCache.put(new Element(key, format))
        }
        return format
    }
}
