package utils

import org.apache.poi.hssf.usermodel.HSSFCell
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.xssf.usermodel.XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFWorkbook

/**
 * Classe que extrai dados de arquivos XLS
 * @author Goran Ehrsson
 */
class ExcelBuilder {

	def workbook
	def rotulos
	def linha

	ExcelBuilder(InputStream inputStream, String nomeDoArquivo) {

		HSSFRow.metaClass.getAt = {int idx ->
			def cell = delegate.getCell(idx)
			if(! cell) {
				return null
			}
			def value
			switch(cell.cellType) {
				case HSSFCell.CELL_TYPE_NUMERIC:
					if(HSSFDateUtil.isCellDateFormatted(cell)) {
						value = cell.dateCellValue
					} else {
						value = cell.numericCellValue
					}
					break
				case HSSFCell.CELL_TYPE_BOOLEAN:
					value = cell.booleanCellValue
					break
				default:
					value = cell.stringCellValue
					break
			}
			return value
		}
		
		XSSFRow.metaClass.getAt = {int idx ->
			def cell = delegate.getCell(idx)
			if(! cell) {
				return null
			}
			def value
			switch(cell.cellType) {
				case XSSFCell.CELL_TYPE_NUMERIC:
					if(DateUtil.isCellDateFormatted(cell)) {
						value = cell.dateCellValue
					} else {
						value = cell.numericCellValue
					}
					break
				case XSSFCell.CELL_TYPE_BOOLEAN:
					value = cell.booleanCellValue
					break
				default:
					value = cell.stringCellValue
					break
			}
			return value
		}


		if (nomeDoArquivo.toLowerCase().endsWith(".xls")) {
			workbook = new HSSFWorkbook(inputStream)
		} else {
			workbook = new XSSFWorkbook(inputStream)
		}
	}

	def getFolha(indice) {
		def sheet
		if(! indice) indice = 0
		if(indice instanceof Number) {
			sheet = workbook.getSheetAt(indice)
		} else if(indice ==~ /^\d+$/) {
			sheet = workbook.getSheetAt(Integer.valueOf(indice))
		} else {
			sheet = workbook.getSheet(indice)
		}
		return sheet
	}

	def celula(indice) {
		if(rotulos && (indice instanceof String)) {
			indice = rotulos.indexOf(indice.toLowerCase())
		}
		return linha[indice];
	}

	def getLinhas() {
		return getFolha(0).rowIterator();
	}

	def propriedadeInexistente(String name) {
		celula(name)
	}

	def eachLine(Map params = [:], Closure closure) {
		def deslocamento = params.deslocamento ?: 0
		def max = params.max ?: 9999999
		def folha = getFolha(params.folha)
		def linhas = folha.rowIterator()
		def linesRead = 0

		if(params.rotulos) {
			rotulos = linhas.next().collect{it.toString().toLowerCase()}
		}
		deslocamento.times{ linhas.next() }

		closure.setDelegate(this)

		while(linhas.hasNext() && linesRead++ < max) {
			linha = linhas.next()
			closure.call(linha)
		}
	}
}