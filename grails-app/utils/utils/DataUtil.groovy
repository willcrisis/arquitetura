package utils

import java.text.ParseException
import java.text.SimpleDateFormat

class DataUtil {
    static boolean ehDataValida(String data, String formato = 'dd/MM/yyyy') {
        if (!data) {
            return false
        }

        try {
            Date dataParse = converterDataString(data.trim(), formato);
            Date dataBaseInferior = converterDataString("01/01/1900", "dd/MM/yyyy");
            Date dataBaseSuperior = converterDataString("31/12/2099", "dd/MM/yyyy");
            if (dataParse) {
                if (dataParse.before(dataBaseInferior) || dataParse.after(dataBaseSuperior)) {
                    return false
                }
                return true
            }
            return false;
        } catch (ParseException pe) {
            return false;
        }
    }

    static boolean ehPeriodoValido(String dataInicio, String dataFim, String formato = 'dd/MM/yyyy') {
        return ehPeriodoValido(converterDataString(dataInicio, formato), converterDataString(dataFim, formato))
    }

    static boolean ehPeriodoValido(Date dataInicio, Date dataFim) {
        return dataInicio <= dataFim
    }

    static Date converterDataString(String data, String mascara = 'dd/MM/yyyy') {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mascara);
        dateFormat.setLenient(false);
        return dateFormat.parse(data)
    }

    static Date converterDataStringHoraFinal(String data, String mascara = 'dd/MM/yyyy') {
        Date dataConvertida = converterDataString(data, mascara)
        Calendar cal = Calendar.instance
        cal.setTime(dataConvertida)
        setarValores(cal, 23, 59, 59, 999)
        return cal.time
    }

    static Date ultimoDiaDoMes(Date data) {
        Calendar cal = Calendar.instance
        cal.setTime(data)
        setarValores(cal, 23, 59, 59, 999, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        return cal.time
    }

    static Date primeiroDiaDoMes(Date data) {
        Calendar cal = Calendar.instance
        cal.setTime(data)
        setarValores(cal, 0, 0, 0, 0, 1)
        return cal.time
    }

    static Date dataAtualHoraZerada() {
        Calendar cal = Calendar.instance
        setarValores(cal, 0, 0, 0, 0)
        return cal.time
    }

    private static setarValores(Calendar cal, int hora, int minuto, int segundo, int milissegundo, Integer dia = null) {
        cal.set(Calendar.HOUR_OF_DAY, hora)
        cal.set(Calendar.MINUTE, minuto)
        cal.set(Calendar.SECOND, segundo)
        cal.set(Calendar.MILLISECOND, milissegundo)
        if (dia) {
            cal.set(Calendar.DAY_OF_MONTH, dia)
        }
    }
}
