package utils

import java.math.RoundingMode

class TratadorDeParametros {

    static Long somenteNumerosLong(valor){
        return !valor ? null : toLong((valor =~ /\D/).replaceAll(GString.EMPTY));
    }

    static Integer somenteNumerosInteger(valor){
        return !valor ? null : toInteger((valor =~ /\D/).replaceAll(GString.EMPTY));
    }

    static Long toLong(valor){
        return valor && valor.toString().isNumber() ? Long.valueOf(valor) : null;
    }

    static Integer toInteger(valor){
        return valor != null && valor.toString().isNumber() ? Integer.valueOf(valor) : null;
    }

    static Date toDateDDMMYYYY(valor) {
        return (valor && DataUtil.ehDataValida(valor)) ? DataUtil.converterDataString(valor) : null;
    }

    static Date toDateMMYYYY(valor) {
        return (valor && DataUtil.ehDataValida(valor, "MM/yyyy")) ? DataUtil.converterDataString(valor, 'MM/yyyy') : null;
    }

    static Date toDateSerial(valor) {
        return (valor && DataUtil.ehDataValida(valor, "yyyy-MM-dd'T'HH:mm:ss'Z'")) ? DataUtil.converterDataString(valor, "yyyy-MM-dd'T'HH:mm:ss'Z'") : null;
    }

    static Short toShort(valor){
        return valor && valor.toString().isNumber() ? Short.valueOf(valor) : null;
    }

    static BigDecimal toBigDecimal(valor){
        return  valor && valor.toString().isNumber() ? new BigDecimal(valor) : null;
    }

    @SuppressWarnings('BooleanMethodReturnsNull')
    static Boolean toBoolean(valor){
        return  "true".equals(valor) || "false".equals(valor) ? Boolean.valueOf(valor) : null;
    }
    /**
     * toBooleanOn
     * O checkbox envia "on" quando está selecionado.
     * @param valor
     * @return primitivo booleano.
     * @author diego.morais
     */
    static boolean toBooleanOn(valor){
        final String variavel =  valor == null ? "" : valor.toString();
        return  "true".equals(variavel) || "on".equals(variavel) ? true : false;
    }

    /**
     * Transforma um valor determinado valor em BigDecimal.
     *
     * @param valor a o valor a ser convertido
     * @author ruither.silva
     * */
    static BigDecimal stringToBigDecimal(String valor) {
        if(!valor){
            return null
        }

        def valorSemPontos = valor.replace('.', '').replace(',', '.')

        return new BigDecimal(valorSemPontos);
    }

    /**
     * Formata um BigDecimal arrendondando-o para 2 casas decimais e colocando a vírgula como separador de decimal.
     * */
    static String bigDecimalToString(BigDecimal valor) {
        def valorArredondado = valor.setScale(2, RoundingMode.HALF_UP);
        return valorArredondado.toString().replace('.', ',');
    }
}
