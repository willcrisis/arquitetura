package utils

class ArquivoUtil {
    public static File criarPastaSeNaoExiste(String caminho) {
        def pasta = new File(caminho)
        if (!pasta.exists()) {
            pasta.mkdirs()
        }
        return pasta
    }
}
