<%@ page import="arquitetura.configuracao.Configuracao" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'configuracao.label', default: 'Configuracao')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'configuracao.plural.label', default: 'Configuracao')}" scope="request" />
		<g:set var="instancia" value="${configuracaoInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:render template="form"/>
	</body>
</html>