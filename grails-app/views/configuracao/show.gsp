
<%@ page import="arquitetura.configuracao.Configuracao" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'configuracao.label', default: 'Configuracao')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'configuracao.plural.label', default: 'Configuracaos')}" scope="request" />
	<g:set var="instancia" value="${configuracaoInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="configuracao.chave.label" default="Chave" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${configuracaoInstance}" field="chave"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="configuracao.valor.label" default="Valor" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${configuracaoInstance}" field="valor"/>
				
			</p>
		</div>
	</div>
	
</body>
</html>