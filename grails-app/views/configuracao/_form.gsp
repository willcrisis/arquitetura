<%@ page import="arquitetura.configuracao.Configuracao" %>



<div class="form-group">
	<label for="chave" class="col-lg-2 control-label">
		<g:message code="configuracao.chave.label" default="Chave"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="chave" required="" value="${configuracaoInstance?.chave}"/>

	</div>
</div>

<div class="form-group">
	<label for="valor" class="col-lg-2 control-label">
		<g:message code="configuracao.valor.label" default="Valor"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="valor" required="" value="${configuracaoInstance?.valor}"/>

	</div>
</div>

