
<%@ page import="arquitetura.configuracao.Configuracao" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'configuracao.label', default: 'Configuracao')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'configuracao.plural.label', default: 'Configuracaos')}" scope="request" />
		<g:set var="domainName" value="arquitetura.configuracao.Configuracao" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						
						<g:sortableColumn property="chave" title="${message(code: 'configuracao.chave.label', default: 'Chave')}" params="${filterParams}" />
						
						<g:sortableColumn property="valor" title="${message(code: 'configuracao.valor.label', default: 'Valor')}" params="${filterParams}" />
						
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${configuracaoInstanceList}" status="i" var="configuracaoInstance">
					<tr>
						
						<td><g:link action="show" id="${configuracaoInstance.id}">${fieldValue(bean: configuracaoInstance, field: "chave")}</g:link></td>
						
						<td>${fieldValue(bean: configuracaoInstance, field: "valor")}</td>
						
						<td class="acoes">
							<will:editLink id="${configuracaoInstance.id}" />
							<will:deleteConfirmButton id="${configuracaoInstance.id}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${configuracaoInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>