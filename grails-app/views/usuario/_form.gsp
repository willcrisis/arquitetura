<%@ page import="arquitetura.seguranca.Usuario" %>

<div class="form-group">
	<label for="nomeCompleto" class="col-lg-2 control-label">
		<g:message code="usuario.nomeCompleto.label" default="Nome Completo"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="nomeCompleto" required="" value="${usuarioInstance?.nomeCompleto}"/>
	</div>
</div>

<div class="form-group">
	<label for="email" class="col-lg-2 control-label">
		<g:message code="usuario.email.label" default="Email"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="email" required="" value="${usuarioInstance?.email}"/>
	</div>
</div>

<div class="form-group">
	<label for="username" class="col-lg-2 control-label">
		<g:message code="usuario.username.label" default="Username"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="username" required="" value="${usuarioInstance?.username}"/>
	</div>
</div>

<div class="form-group">
	<label for="password" class="col-lg-2 control-label">
		<g:message code="usuario.password.label" default="Password"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<g:if test="${novo}">
			<will:passwordField name="password" required="" />
		</g:if><g:else>
			<will:passwordField name="password" />
		</g:else>
	</div>
</div>

<div class="form-group">
	<label for="accountExpired" class="col-lg-2 control-label">
		<g:message code="usuario.accountExpired.label" default="Account Expired"/>
	</label>
	<div class="col-lg-4">
		<will:checkBox name="accountExpired" value="${usuarioInstance?.accountExpired}" />
	</div>
</div>

<div class="form-group">
	<label for="accountLocked" class="col-lg-2 control-label">
		<g:message code="usuario.accountLocked.label" default="Account Locked"/>
	</label>
	<div class="col-lg-4">
		<will:checkBox name="accountLocked" value="${usuarioInstance?.accountLocked}" />
	</div>
</div>

<div class="form-group">
	<label for="enabled" class="col-lg-2 control-label">
		<g:message code="usuario.enabled.label" default="Enabled"/>
	</label>
	<div class="col-lg-4">
		<will:checkBox name="enabled" value="${usuarioInstance?.enabled}" />
	</div>
</div>

<div class="form-group">
	<label for="passwordExpired" class="col-lg-2 control-label">
		<g:message code="usuario.passwordExpired.label" default="Password Expired"/>
	</label>
	<div class="col-lg-4">
		<will:checkBox name="passwordExpired" value="${usuarioInstance?.passwordExpired}" />
	</div>
</div>

<div class="form-group">
	<label for="permissoes" class="col-lg-2 control-label">
		<g:message code="usuarioCommand.permissoes.label" default="Permissoes" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-6">
		<will:select id="permissoes" name="permissoes" from="${arquitetura.seguranca.Papel.list()}" value="${usuarioInstance?.permissoes?.id}" multiple="multiple" optionKey="id" optionValue="authority" />
	</div>
</div>

