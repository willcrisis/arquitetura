
<%@ page import="arquitetura.seguranca.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuarios')}" />
		<title><g:message code="usuario.minhaConta.label" /></title>
	</head>
	<body>
        <div id="show-cliente" class="container" role="main">
            <g:form class="form-horizontal" role="form" >
                
                <g:if test="${usuarioInstance?.nomeCompleto != null}">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            <g:message code="usuario.nomeCompleto.label" default="nomeCompleto" />
                        </label>
                        <div class="col-lg-4">
                            <p class="form-control-static">
                                
                                <g:fieldValue bean="${usuarioInstance}" field="nomeCompleto"/>
                                
                            </p>
                        </div>
                    </div>
                </g:if>
                
                <g:if test="${usuarioInstance?.email != null}">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            <g:message code="usuario.email.label" default="email" />
                        </label>
                        <div class="col-lg-4">
                            <p class="form-control-static">
                                
                                <g:fieldValue bean="${usuarioInstance}" field="email"/>
                                
                            </p>
                        </div>
                    </div>
                </g:if>
                
                <g:if test="${usuarioInstance?.username != null}">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            <g:message code="usuario.username.label" default="Username" />
                        </label>
                        <div class="col-lg-4">
                            <p class="form-control-static">
                                
                                <g:fieldValue bean="${usuarioInstance}" field="username"/>
                                
                            </p>
                        </div>
                    </div>
                </g:if>

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <g:message code="usuario.password.label" default="Password" />
                    </label>
                    <div class="col-lg-4">
                        <div class="col-lg-12">
                        <will:iframe id="senha${usuarioInstance.id}" link="${createLink(controller: 'usuario', action: 'alterarSenhaPopup', id: usuarioInstance?.id)}"
                            icone="pencil" linkOk="${createLink(controller: 'usuario', action: 'minhaConta', id: usuarioInstance.id)}"
                            titulo="${message(code: 'usuario.alterar.senha.label')}" altura="380"><g:message code="usuario.alterar.senha.label" /> </will:iframe>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <g:message code="usuario.vincularRedesSociais.label" default="Vincular redes sociais" />
                    </label>
                    <div class="col-lg-4">
                        <div class="col-lg-12">
                            <oauth:connect provider="google" class="btn btn-material-red btn-lg btn-primary"><i class="fa fa-fw fa-google"></i>Google</oauth:connect>
                        </div>
                        <div class="col-lg-12">
                            <oauth:connect provider="twitter" class="btn btn-material-light-blue btn-lg btn-primary"><i class="fa fa-fw fa-twitter"></i>Twitter</oauth:connect>
                        </div>
                        <div class="col-lg-12">
                            <oauth:connect provider="facebook" class="btn btn-material-blue btn-lg btn-primary"><i class="fa fa-fw fa-facebook"></i>Facebook</oauth:connect>
                        </div>
                    </div>
                </div>

                <g:hiddenField name="id" value="${usuarioInstance?.id}" />
			</g:form>
		</div>
	</body>
</html>
