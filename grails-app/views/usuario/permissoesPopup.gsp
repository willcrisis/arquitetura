<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="popup">
    <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
    <g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuarios')}" />
    <g:set var="instancia" value="${usuarioInstance}" scope="request" />
    <title><g:message code="default.new.label" args="[entityName]" /></title>
</head>
<body>
<div id="create-menu" class="container" role="main">
    <g:form action="salvarPopup" class="form-horizontal" role="form" >
        <g:hiddenField name="id" value="${usuarioInstance?.id}" />
        <g:hiddenField name="versao" value="${usuarioInstance?.versao}" />
        <div class="form-group">
            <label for="permissoes" class="col-lg-2 control-label">
                <g:message code="usuarioCommand.permissoes.label" default="Permissoes" />
            </label>
            <div class="col-lg-6">
                <will:select id="permissoes" name="permissoes" from="${arquitetura.seguranca.Papel.list()}" value="${usuarioInstance?.permissoes?.id}" multiple="multiple" optionKey="id" optionValue="authority" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <will:submitButton action="salvarPermissoes" />
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
