<%@ page contentType="text/html"%>
<html>
<head>
    <style type="text/css">
        body {

        }
        .container {

        }
        .align-right {
            text-align: right;
        }
        .align-left {
            text-align: left;
        }
        .align-center {
            text-align: center;
        }

        .align-justify {
            text-align: justify;
        }
        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            padding-right: 15px;
            padding-left: 15px;
        }
        .navbar-default {
            background-color: #009688;
            color: rgba(255, 255, 255, .84);
        }
    </style>
</head>

<body style="font-family: RobotoDraft,Roboto,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 12px;font-weight: 300;line-height: 1.42857143;color: #333;">
<div style="text-align: center;">
    <div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;min-width: 500px;max-width: 750px;">
        <div style="position: relative;min-height: 50px;margin-bottom: 20px;border: 1px solid transparent;padding-right: 15px;padding-left: 15px;background-color: #009688;color: rgba(255, 255, 255, .84);text-align: left;">
            <a href="${urlServidor}"><img src="cid:logo" /></a>
        </div>
        <div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;min-width: 500px;max-width: 750px;">
        <div style="text-align: justify;">
            <p style="margin: 0 0 10px;">Olá, ${nome}!</p>
            <p style="margin: 0 0 10px;">Bem-vindo ao <a style="color: #009688;text-decoration: none;background-color: transparent;" href="${urlServidor}">abasteça.me</a>.</p>
            <p style="margin: 0 0 10px;">A partir de agora, você poderá ter total controle de tudo o que é gasto com seu veículo, seja um carro, uma moto, um navio, uma nave espacial... </p>
            <p style="margin: 0 0 10px;">Comece cadastrando seu veículo. Dê um nome, diga qual a marca e modelo e, se preferir, dê algumas informações adicionais. Não se preocupe. Seus dados não podem ser acessados por mais ninguém sem o seu consentimento.
                A única forma de outras pessoas verem os dados do seu veículo é se você escolher compartilhar.</p>
            <p style="margin: 0 0 10px;">Depois, na hora em que for abastecer, é só informar os dados que nossos robôs muito bem treinados irão se encarregar do resto.</p>
            <p style="margin: 0 0 10px;">Caso tenha alguma dúvida, você pode ler o nosso <a style="color: #009688;text-decoration: none;background-color: transparent;" href="${urlServidor}/faq">FAQ</a> ou <a style="color: #009688;text-decoration: none;background-color: transparent;" href="mailto:suporte@abasteca.me">entar em contato</a>.</p>
            <p style="margin: 0 0 10px;">Encontrou algum bug ou tem alguma sugestão? Use o ícone de Feedback e me deixe saber o que se passa.</p>
            <p style="margin: 0 0 10px;">Espero que goste da experiência!</p>
            <p style="margin: 0 0 10px;"></p>
            <p style="margin: 0 0 10px;text-align: right;">Atenciosamente,<br>Willian Krause</p>
        </div>
        </div>
    </div>
</div>
</body>
</html>
