<%@ page import="arquitetura.seguranca.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="edit">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuario')}" scope="request" />
		<g:set var="instancia" value="${usuarioInstance}" scope="request" />
		<title><g:message code="default.edit.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:hiddenField name="id" value="${usuarioInstance?.id}" />
		<g:hiddenField name="versao" value="${usuarioInstance?.versao}" />
		<g:render template="form"/>
	</body>
</html>