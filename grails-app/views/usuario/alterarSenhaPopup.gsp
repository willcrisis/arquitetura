<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="popup">
    <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
    <g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuarios')}" />
    <g:set var="instancia" value="${usuarioInstance}" scope="request" />
    <title><g:message code="default.new.label" args="[entityName]" /></title>
</head>
<body>
<div id="create-menu" class="container" role="main">
    <g:form action="salvarPopup" class="form-horizontal" role="form" >
        <g:hiddenField name="id" value="${usuarioInstance?.id}" />
        <g:hiddenField name="versao" value="${usuarioInstance?.versao}" />
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">
                <g:message code="usuario.senhaAtual.label" default="Password" />
                <span class="required-indicator">*</span>
            </label>
            <div class="col-lg-4">
                <will:passwordField name="password" required="" />
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">
                <g:message code="usuario.novaSenha.label" default="Password" />
                <span class="required-indicator">*</span>
            </label>
            <div class="col-lg-4">
                <will:passwordField name="novaSenha" required="" />
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">
                <g:message code="usuario.confirmacaoSenha.label" default="Password" />
                <span class="required-indicator">*</span>
            </label>
            <div class="col-lg-4">
                <will:passwordField name="confirmacaoSenha" required="" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <will:submitButton action="alterarSenha" />
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
