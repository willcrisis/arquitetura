
<%@ page import="arquitetura.seguranca.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuarios')}" scope="request" />
		<g:set var="domainName" value="arquitetura.seguranca.Usuario" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
		<script type="text/javascript">

		</script>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>

						<g:sortableColumn property="username"
										  title="${message(code: 'usuario.username.label', default: 'Username')}"/>

						<g:sortableColumn property="accountExpired"
										  title="${message(code: 'usuario.accountExpired.label', default: 'Account Expired')}"/>

						<g:sortableColumn property="accountLocked"
										  title="${message(code: 'usuario.accountLocked.label', default: 'Account Locked')}"/>

						<g:sortableColumn property="enabled"
										  title="${message(code: 'usuario.enabled.label', default: 'Enabled')}"/>

						<g:sortableColumn property="passwordExpired"
										  title="${message(code: 'usuario.passwordExpired.label', default: 'Password Expired')}"/>

						<th><g:message code="sistema.acoes.label"/></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${usuarioInstanceList}" status="i" var="usuarioInstance">
					<tr>

						<td><g:link action="show"
									id="${usuarioInstance.id}">${fieldValue(bean: usuarioInstance, field: "username")}</g:link></td>

						<td>
							<will:actionButton type="button" class="btn btn-flat acoes btn-${usuarioInstance.accountExpired ? 'danger' : 'success'}" icone="${usuarioInstance.accountExpired ? 'check' : 'times'}" data-toggle="modal" data-target="#expirada${usuarioInstance.id}" title="${message(code: 'usuario.alternar.tooltip')}" />
							<will:confirmacao id="expirada${usuarioInstance.id}"
										   mensagem="${message(code: 'usuario.alternar.confirmar.message', args: [message(code: 'usuario.accountExpired.label', default: 'Account Expired')])}"
										   linkConfirma="${createLink(action: 'alternar', id: usuarioInstance.id, params: [campo: 'accountExpired', versao: usuarioInstance.version])}"/>
						</td>

						<td>
							<will:actionButton type="button" class="btn btn-flat acoes btn-${usuarioInstance.accountLocked ? 'danger' : 'success'}" icone="${usuarioInstance.accountLocked ? 'check' : 'times'}" data-toggle="modal" data-target="#bloqueada${usuarioInstance.id}" title="${message(code: 'usuario.alternar.tooltip')}" />
							<will:confirmacao id="bloqueada${usuarioInstance.id}"
										   mensagem="${message(code: 'usuario.alternar.confirmar.message', args: [message(code: 'usuario.accountLocked.label', default: 'Account Locked')])}"
										   linkConfirma="${createLink(action: 'alternar', id: usuarioInstance.id, params: [campo: 'accountLocked', versao: usuarioInstance.version])}"/>
						</td>

						<td>
							<will:actionButton type="button" class="btn btn-flat acoes btn-${usuarioInstance.enabled ? 'success' : 'danger'}" icone="${usuarioInstance.enabled ? 'check' : 'times'}" data-toggle="modal" data-target="#ativada${usuarioInstance.id}" title="${message(code: 'usuario.alternar.tooltip')}" />
							<will:confirmacao id="ativada${usuarioInstance.id}"
										   mensagem="${message(code: 'usuario.alternar.confirmar.message', args: [message(code: 'usuario.enabled.label', default: 'Enabled')])}"
										   linkConfirma="${createLink(action: 'alternar', id: usuarioInstance.id, params: [campo: 'enabled', versao: usuarioInstance.version])}"/>
						</td>

						<td>
							<will:actionButton type="button" class="btn btn-flat acoes btn-${usuarioInstance.passwordExpired ? 'danger' : 'success'}" icone="${usuarioInstance.passwordExpired ? 'check' : 'times'}" data-toggle="modal" data-target="#senhaExpirada${usuarioInstance.id}" title="${message(code: 'usuario.alternar.tooltip')}" />
							<will:confirmacao id="senhaExpirada${usuarioInstance.id}"
										   mensagem="${message(code: 'usuario.alternar.confirmar.message', args: [message(code: 'usuario.passwordExpired.label', default: 'Password Expired')])}"
										   linkConfirma="${createLink(action: 'alternar', id: usuarioInstance.id, params: [campo: 'passwordExpired', versao: usuarioInstance.version])}"/>
						</td>
						
						<td class="acoes">
							<will:editLink id="${usuarioInstance.id}" />
							<will:deleteConfirmButton id="${usuarioInstance.id}" />
							<will:iframe icone="lock" class="acoes" id="permissoes${usuarioInstance.id}" titulo="${message(code: 'usuarioCommand.permissoes.label')}"
										 linkOk="${createLink(controller: 'usuario', action: 'list', params: params)}"
										 link="${createLink(controller: 'usuario', action: 'permissoesPopup', id: usuarioInstance?.id)}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${usuarioInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>