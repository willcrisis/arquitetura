<%@ page import="arquitetura.seguranca.Usuario" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="login">
    <title><g:message code="sistema.nome.label"/></title>
</head>

<body>
<g:form class="form-horizontal form-signin" role="form" action="registrar" >
    <div class="container" role="main">
        <div class="form-group">
            <div class="col-lg-6">
                <div class="col-lg-12">
                    <h2><g:message code="usuario.registrar.label" /></h2>
                </div>
                <div class="col-lg-12">
                    <will:textField name="nomeCompleto" required="" value="${usuarioInstance?.nomeCompleto}" placeholder="${message(code: 'usuario.nomeCompleto.label')}"/>
                </div>
                <div class="col-lg-12">
                    <will:textField name="email" required="" value="${usuarioInstance?.email}" placeholder="${message(code: 'usuario.email.label')}" email="true"/>
                </div>
                <div class="col-lg-12">
                    <will:passwordField name="novaSenha" required="" placeholder="${message(code: 'usuario.password.label')}" />
                </div>
                <div class="col-lg-12">
                    <will:passwordField name="confirmacaoSenha" required="" placeholder="${message(code: 'usuario.confirmacaoSenha.label')}" />
                </div>
                <div class="col-lg-12">
                    <will:submitButton action="registrar"><g:message code="usuario.registrar.label" /></will:submitButton>
                </div>
            </div>
            <div class="col-lg-6">
                <g:if test="${!session['oAuthID']}">
                    <div class="col-lg-12">
                        <p><g:message code="default.registroRedesSociais.label" /> </p>
                    </div>
                    <div class="col-lg-12">
                        <oauth:connect provider="google" class="btn btn-material-red btn-lg btn-primary btn-block"><i class="fa fa-fw fa-google"></i>Google</oauth:connect>
                    </div>
                    <div class="col-lg-12">
                        <oauth:connect provider="twitter" class="btn btn-material-light-blue btn-lg btn-primary btn-block"><i class="fa fa-fw fa-twitter"></i>Twitter</oauth:connect>
                    </div>
                    <div class="col-lg-12">
                        <oauth:connect provider="facebook" class="btn btn-material-blue btn-lg btn-primary btn-block"><i class="fa fa-fw fa-facebook"></i>Facebook</oauth:connect>
                    </div>
                </g:if>
            </div>
        </div>
    </div>
</g:form>
</body>
</html>