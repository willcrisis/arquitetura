
<%@ page import="arquitetura.seguranca.Usuario" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'usuario.plural.label', default: 'Usuarios')}" scope="request" />
	<g:set var="instancia" value="${usuarioInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.nomeCompleto.label" default="nomeCompleto" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:fieldValue bean="${usuarioInstance}" field="nomeCompleto"/>

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.email.label" default="email" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:fieldValue bean="${usuarioInstance}" field="email"/>

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.username.label" default="Username" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:fieldValue bean="${usuarioInstance}" field="username"/>

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.accountExpired.label" default="Account Expired" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:formatBoolean boolean="${usuarioInstance?.accountExpired}" />

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.accountLocked.label" default="Account Locked" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:formatBoolean boolean="${usuarioInstance?.accountLocked}" />

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.enabled.label" default="Enabled" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:formatBoolean boolean="${usuarioInstance?.enabled}" />

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuario.passwordExpired.label" default="Password Expired" />
	</label>
	<div class="col-lg-4">
		<p class="form-control-static">

			<g:formatBoolean boolean="${usuarioInstance?.passwordExpired}" />

		</p>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">
		<g:message code="usuarioCommand.permissoes.label" default="Permissoes"/>
	</label>

	<div class="col-lg-4">
		<p class="form-control-static">

			<g:each in="${usuarioInstance.permissoes}" var="p">
				${p?.authority}<br />
			</g:each>

		</p>
	</div>
</div>
	
</body>
</html>