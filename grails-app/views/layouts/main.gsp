<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><g:layoutTitle default="Grails"/></title>
    <asset:javascript src="application.js"/>
    <asset:stylesheet src="application.css"/>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
	<g:layoutHead/>
	<ua:trackPageview />
</head>
<body>

<willcrisis:menuPrincipal>
    <g:render template="/menuPrincipal/menu_topo" />
</willcrisis:menuPrincipal>
<g:render template="/layouts/messages" />
<g:layoutBody/>

%{--<g:render template="/layouts/loading" />--}%
<g:render template="/layouts/menus/menu_rodape" />
</body>
</html>