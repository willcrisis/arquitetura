<script>
    <g:if test="${flash.message}">
        exibirMensagem('${flash.message}');
    </g:if>
    <g:if test="${params.message}">
        exibirMensagem('${params.message}');
    </g:if>

    <g:if test="${instancia}">
        <g:hasErrors bean="${instancia}">
            <g:eachError bean="${instancia}" var="error">
                exibirMensagem('${message(error: error)}', 'danger');
            </g:eachError>
        </g:hasErrors>
    </g:if>

    <g:if test="${params.errors}">
        <g:each in="${params.errors}" var="error">
    exibirMensagem('${message(error: error)}', 'danger');
        </g:each>
    </g:if>
</script>



