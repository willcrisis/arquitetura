<g:applyLayout name="main">
    <html>
        <head>
            <title><g:layoutTitle/></title>
            <g:layoutHead/>
        </head>
        <body>
            <g:form useToken="true" class="form-horizontal" role="form" action="save" enctype="multipart/form-data">
                <g:hiddenField name="controladorRetorno" value="${params.controladorRetorno}" />
                <g:hiddenField name="actionRetorno" value="${params.actionRetorno}" />
                <div id="create" class="container-fluid" role="main">
                    <div class="page well">
                        <h1><g:message code="default.new.male.label" args="[entityName]"/></h1>
                        <g:layoutBody/>
                    </div>
                </div>

                <div class="action-buttons">
                    <g:render template="/layouts/menus/submenu_create" />
                </div>
            </g:form>
        </body>
    </html>
</g:applyLayout>