<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="br" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="br" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="br" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="br" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="br" class="no-js"><!--<![endif]-->
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Willian Krause">
        <asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

        <title><g:message code="sistema.nome.label"/></title>

        <asset:javascript src="application.js"/>
        <asset:stylesheet src="application.css"/>

        <style>
    body {
        padding-top: 60px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
        </style>

    </head>

    <body>
        <g:render template="/layouts/menus/menu_login" />

        <g:layoutBody/>

        <div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
    </body>
</html>
