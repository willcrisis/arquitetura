<%@ page import="arquitetura.menu.Menu" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="popup">
	</head>
	<body>
		<div id="mensagem" class="container-fluid" role="main">
            ${mensagem}
		</div>
	</body>
</html>
