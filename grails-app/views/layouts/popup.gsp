<!DOCTYPE html>
<html>
<head>
	<title><g:layoutTitle default="Grails"/></title>
    <asset:javascript src="application.js"/>
    <asset:stylesheet src="application.css"/>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
	<g:layoutHead/>
	<style>
		body {
			padding-top: 0;
		}
	</style>
	<ua:trackPageview />
</head>
<body style="background: white">
<g:render template="/layouts/messages" />
<g:layoutBody/>

%{--<g:render template="/layouts/loading" />--}%
</body>
</html>