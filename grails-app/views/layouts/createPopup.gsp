<g:applyLayout name="popup">
    <html>
        <head>
            <title><g:layoutTitle/></title>
            <g:layoutHead/>
        </head>
        <body>
            <g:form useToken="true" class="form-horizontal" role="form" action="save" enctype="multipart/form-data">
                <g:hiddenField name="controladorRetorno" value="${params.controladorRetorno}" />
                <g:hiddenField name="actionRetorno" value="${params.actionRetorno}" />
                <div id="create" class="container-fluid" role="main">
                    <g:layoutBody/>
                </div>

                <div class="action-buttons">
                    <g:render template="/layouts/menus/submenu_create" />
                </div>
            </g:form>
        </body>
    </html>
</g:applyLayout>