<g:applyLayout name="popup">
    <html>
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
    </head>
    <body>
    <div id="show" class="container-fluid" role="main">
        <g:form class="form-horizontal" role="form" action="delete" >
            <g:layoutBody/>
            <g:hiddenField name="id" value="${instancia?.id}" />
            <div class="action-buttons">
                <g:render template="/layouts/menus/submenu_show" />
            </div>
        </g:form>
    </div>
    </body>
    </html>
</g:applyLayout>