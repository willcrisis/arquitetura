<div class="panel panel-default">
    <div class="panel-heading">
        <a class="accordion-toggle" data-toggle="collapse" href="#filtros">
            <h3 class="panel-title"><g:message code="default.filterPanel.label"/></h3>
        </a>
    </div>

    <filterpane:currentCriteria domainBean="${dominio}" dateFormat="${[title: 'dd/MM/yyyy']}"
                                fullAssociationPathFieldNames="no"/>

    <div id="filtros" class="panel-collapse collapse ${aberto ? 'in' : ''}" style="padding-top: 10px;">
        <filterpane:filterPane domain="${dominio}"
                               filterProperties="${filterProperties}"
                               associatedProperties="${associatedProperties}"
                               excludeProperties="${excludeProperties}"
                               filterPropertyValues="${filterPropertyValues}"
                               visible="true" showTitle="false" showSortPanel="false" showButtons="true" />
    </div>
</div>