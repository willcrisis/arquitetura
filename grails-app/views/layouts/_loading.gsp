<div id="overlayLoading"></div>
<div id="loading">
    <g:img file="spinner.gif" />
    Aguarde...
</div>
<script>
    $("#overlayLoading").css({opacity : 0.5});
    $(document).ajaxStart(function() {
        aguardar();
    }).ajaxStop(function() {
        liberar();
    });

    function aguardar() {
        $("#overlayLoading").show();
        $("#loading").show();
    }

    function liberar() {
        $("#overlayLoading").hide();
        $("#loading").hide();
    }
</script>