<g:applyLayout name="main">
    <html>
        <head>
            <title><g:layoutTitle/></title>
            <g:layoutHead/>
        </head>
        <body>
            <g:form action="update" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                <g:hiddenField name="controladorRetorno" value="${params.controladorRetorno}" />
                <g:hiddenField name="actionRetorno" value="${params.actionRetorno}" />
                <div id="edit" class="container-fluid" role="main">
                    <g:layoutBody/>
                </div>
                <div class="action-buttons">
                    <g:render template="/layouts/menus/submenu_edit" />
                </div>
            </g:form>
        </body>
    </html>
</g:applyLayout>