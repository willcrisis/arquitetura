<!DOCTYPE html>
<html>
<head>
	<title><g:layoutTitle default="Grails"/></title>
	<asset:stylesheet src="application.css"/>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<g:layoutHead/>
	<asset:javascript src="application.js"/>
	<script type="text/javascript">

	</script>
	<style>
		body {
			padding-top: 0;
		}
	</style>
</head>
<body>
<g:layoutBody/>
</body>
</html>