<g:if test="${pageProperty(name: 'page.sobrescreveSubmenu')?.length()}">
    <g:pageProperty name="page.sobrescreveSubmenu"/>
</g:if><g:else>
    <g:pageProperty name="page.submenu"/>
    <g:if test="${!esconderControles}">
        <will:editButtonFlutuante id="${instancia?.id}" title="${message(code: 'default.button.edit.label')}" />
        <will:deleteConfirmButtonFlutuante id="${instancia.id}" title="${message(code: 'default.button.delete.label')}" />
        <will:linkFlutuante action="list" params="${objeto ? [(objeto): objetoId] : [:]}" title="${message(code: 'default.list.label', args: [entityNamePlural])}" icone="list" />
        %{--<will:linkFlutuante action="create" params="${objeto ? [(objeto): objetoId] : [:]}" title="${message(code: 'default.new.label', args: [entityNamePlural])}" icone="plus" />--}%
    </g:if>
</g:else>