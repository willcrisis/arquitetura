<g:if test="${pageProperty(name: 'page.sobrescreveSubmenu')?.length()}">
    <g:pageProperty name="page.sobrescreveSubmenu"/>
</g:if><g:else>
    <g:pageProperty name="page.submenu"/>
    <will:submitButtonFlutuante action="update" title="${message(code: 'default.button.update.label', args: [entityName])}" />
    <will:deleteConfirmButtonFlutuante id="${instancia.id}" title="${message(code: 'default.button.delete.label')}" />
    <will:linkFlutuante action="list" params="${objeto ? [(objeto): objetoId] : [:]}" title="${message(code: 'default.list.label', args: [entityNamePlural])}" icone="list" />
    <will:linkFlutuante action="create" params="${objeto ? [(objeto): objetoId] : [:]}" title="${message(code: 'default.new.label', args: [entityNamePlural])}" icone="plus" />
</g:else>