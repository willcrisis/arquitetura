<g:if test="${pageProperty(name: 'page.sobrescreveSubmenu')?.length()}">
    <g:pageProperty name="page.sobrescreveSubmenu"/>
</g:if><g:else>
    <will:linkFlutuante icone="plus" action="create" params="${objeto ? [(objeto): objetoId] : [:]}" tipo="primary" title="${message(code: 'default.new.label', args: [entityName])}" />
    <g:pageProperty name="page.submenu"/>
</g:else>
