<div class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${createLink(uri: '/')}"><g:message code="sistema.nome.label" /> </a>
        </div>
    </div>
</div>