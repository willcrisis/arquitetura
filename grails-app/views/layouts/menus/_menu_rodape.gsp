<will:mensagem id="feedback" titulo="${message(code: 'bug.popup.title')}" tamanho="lg" >
    <g:render template="/bug/popup"/>
</will:mensagem>

<div class="rodape">
    <a href="#" data-toggle="modal" data-target="#feedback"><i class="fa fa-bug fa-3x"></i></a>
</div>