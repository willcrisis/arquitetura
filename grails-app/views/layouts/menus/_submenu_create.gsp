<g:if test="${pageProperty(name: 'page.sobrescreveSubmenu')?.length()}">
    <g:pageProperty name="page.sobrescreveSubmenu"/>
</g:if><g:else>
    <g:pageProperty name="page.submenu"/>
    <will:submitButtonFlutuante action="save" title="${message(code: 'default.button.create.label', args: [entityName])}" />
    <will:resetButtonFlutuante title="${message(code: 'default.button.clean.label')}" />
    <will:linkFlutuante action="list" params="${objeto ? [(objeto): objetoId] : [:]}" title="${message(code: 'default.list.label', args: [entityNamePlural])}" icone="list" />
</g:else>

