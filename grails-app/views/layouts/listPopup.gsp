<g:applyLayout name="popup">
    <html>
        <head>
            <title><g:layoutTitle/></title>
            <g:layoutHead/>
        </head>

        <body>
            <div id="list" class="container-fluid" role="main">
                <g:if test="${!esconderFiltros}">
                    <g:render template="/layouts/filtros" model="[dominio: domainName, filterProperties: filterProperties,
                                                                  associatedProperties: associatedProperties, excludeProperties: excludeProperties, filterPropertyValues: filterPropertyValues]" />
                </g:if>
                <g:layoutBody/>
            </div>

            <div class="action-buttons">
                <g:render template="/layouts/menus/submenu_list"/>
            </div>
        </body>
    </html>
</g:applyLayout>