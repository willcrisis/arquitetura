
<%@ page import="arquitetura.bug.Bug" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'bug.label', default: 'Bug')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'bug.plural.label', default: 'Bugs')}" scope="request" />
	<g:set var="instancia" value="${bugInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	<g:if test="${bugInstance.situacao?.fechado}">
		<g:set var="esconderControles" value="${true}" scope="request" />
	</g:if>
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.titulo.label" default="Titulo" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${bugInstance}" field="titulo"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.descricao.label" default="Descricao" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${bugInstance}" field="descricao"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.tipo.label" default="Tipo" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${bugInstance}" field="tipo"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.usuario.label" default="Usuario" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				${bugInstance?.usuario?.nomeCompleto}
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.data.label" default="Data" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:formatDate date="${bugInstance?.data}" />
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.situacao.label" default="Situacao" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				<g:message code="${bugInstance.situacao?.label}" />
			</p>
		</div>
	</div>

	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="bug.historico.label" default="Historico" />
		</label>
		<div class="col-lg-4">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
					<tr>
						<th><g:message code="historicoBug.data.label" default="Data" /></th>
						<th><g:message code="historicoBug.situacao.label" default="Situação" /></th>
						<th><g:message code="historicoBug.observacoes.label" default="observacoes" /></th>
					</tr>
					</thead>
					<tbody>
					<g:each in="${bugInstance.historico}" status="i" var="historico">
						<tr>
							<td>${historico?.data?.format('dd/MM/yyyy')}</td>
							<td><g:message code="${historico?.situacao?.label}" /></td>
							<td>${historico?.observacoes}</td>
						</tr>
					</g:each>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>