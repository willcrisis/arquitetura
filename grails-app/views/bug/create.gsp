<%@ page import="arquitetura.bug.Bug" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'bug.label', default: 'Bug')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'bug.plural.label', default: 'Bug')}" scope="request" />
		<g:set var="instancia" value="${bugInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
        <g:render template="form"/>
	</body>
</html>