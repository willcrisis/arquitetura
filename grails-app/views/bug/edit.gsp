<%@ page import="arquitetura.bug.Bug" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="edit">
		<g:set var="entityName" value="${message(code: 'bug.label', default: 'Bug')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'bug.plural.label', default: 'Bug')}" scope="request" />
		<g:set var="instancia" value="${bugInstance}" scope="request" />
		<title><g:message code="default.edit.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:hiddenField name="id" value="${bugInstance?.id}" />
		<g:hiddenField name="version" value="${bugInstance?.version}" />
		<g:render template="form"/>
		<div class="form-group">
			<label for="situacaoBug" class="col-sm-2 control-label">
				<g:message code="bug.situacao.label" default="Tipo"/>
				<span class="required-indicator">*</span>
			</label>
			<div class="col-sm-4">
				<will:select name="situacaoBug" from="${arquitetura.bug.SituacaoBug.list()}" optionKey="id" required="" value="${bugInstance?.idSituacao}" />
			</div>
		</div>
		<div class="form-group">
			<label for="observacoes" class="col-sm-2 control-label">
				<g:message code="historicoBug.observacao.label" default="Descricao"/>
				<span class="required-indicator">*</span>
			</label>
			<div class="col-sm-4">
				<will:textArea name="observacoes" cols="40" rows="5" maxlength="3000" required="" />
			</div>
		</div>
	</body>
</html>