<%@ page contentType="text/html"%>
<html>
<head>
    <meta name='layout' content='mail'/>
    <title><g:message code="bug.cadastro.email.assunto" /> </title>
</head>

<body>
    <g:message code="bug.cadastro.email.introducao" args="[sistema]" />
    <table>
        <tr>
            <td>
                <g:message code="bug.usuario.label" />
            </td>
            <td>
                ${bug?.usuario?.nomeCompleto}
            </td>
        </tr>
        <tr>
            <td>
                <g:message code="bug.titulo.label" />
            </td>
            <td>
                ${bug?.titulo}
            </td>
        </tr>
        <tr>
            <td>
                <g:message code="bug.descricao.label" />
            </td>
            <td>
                ${bug?.descricao}
            </td>
        </tr>
    </table>
    <g:message code="bug.cadastro.email.url" args="[urlServidor, bug?.id]" />
</body>
</html>