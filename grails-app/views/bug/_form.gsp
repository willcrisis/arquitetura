<%@ page import="arquitetura.bug.Bug" %>


<div class="form-group">
	<label for="tipo" class="col-sm-2 control-label">
		<g:message code="bug.tipo.label" default="Tipo"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-4">
		<will:select name="tipo" from="${arquitetura.enums.TipoBugEnum?.values()}" keys="${arquitetura.enums.TipoBugEnum.values()*.id}" required="" value="${bugInstance?.tipo?.id}" />
	</div>
</div>

<div class="form-group">
	<label for="titulo" class="col-sm-2 control-label">
		<g:message code="bug.titulo.label" default="Titulo"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-4">
		<will:textField name="titulo" maxlength="150" required="" value="${bugInstance?.titulo}"/>

	</div>
</div>

<div class="form-group">
	<label for="descricao" class="col-sm-2 control-label">
		<g:message code="bug.descricao.label" default="Descricao"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-4">
		<will:textArea name="descricao" cols="40" rows="5" maxlength="3000" required="" value="${bugInstance?.descricao}"/>

	</div>
</div>