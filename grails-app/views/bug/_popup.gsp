<%@ page import="arquitetura.bug.Bug" %>
<g:form class="form-horizontal" role="form" name="bugForm" onsubmit="return incluirBugAjax('${createLink(controller: 'bug', action: 'ajaxSave')}', 'bugForm')">

<asset:javascript src="bug" />

<div class="form-group">
	<label for="tipo" class="col-sm-3 control-label">
		<g:message code="bug.tipo.label" default="Tipo"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-6">
		<will:select name="tipo" from="${arquitetura.enums.TipoBugEnum?.values()}" keys="${arquitetura.enums.TipoBugEnum.values()*.id}" required="" value="${bugInstance?.tipo?.id}" />
	</div>
</div>

<div class="form-group">
	<label for="titulo" class="col-sm-3 control-label">
		<g:message code="bug.titulo.label" default="Titulo"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-6">
		<will:textField name="titulo" maxlength="150" required="" value="${bugInstance?.titulo}" placeholder="${message(code: 'bug.titulo.placeholder')}"/>

	</div>
</div>

<div class="form-group">
	<label for="descricao" class="col-sm-3 control-label">
		<g:message code="bug.descricao.label" default="Descricao"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-6">
		<will:textArea name="descricao" cols="40" rows="5" maxlength="3000" required="" value="${bugInstance?.descricao}" placeholder="${message(code: 'bug.descricao.placeholder')}"/>

	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-3 col-sm-10">
		<will:submitButton action="null" icone="envelope" class="btn btn-success"><g:message code="bug.enviar.label" /></will:submitButton>
		<will:resetButton id="bugReset"><g:message code="default.button.clean.label" /></will:resetButton>
	</div>
</div>

<p class="form-control-static">

	<g:message code="sistema.rodape.info" args="[meta(name: 'app.version')]" />
</p>

<div class="collapse" id="bugMensagem">
	<div class="well" id="bugMensagemInterna">

	</div>
</div>
</g:form>