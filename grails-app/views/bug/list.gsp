
<%@ page import="arquitetura.bug.SituacaoBug; arquitetura.bug.Bug" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'bug.label', default: 'Bug')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'bug.plural.label', default: 'Bugs')}" scope="request" />
		<g:set var="domainName" value="arquitetura.bug.Bug" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
        <g:set var="excludeProperties" value="descricao" scope="request"/>
        <g:set var="filterPropertyValues" value="${[idSituacao: [values: SituacaoBug.list(), optionKey: 'id']]}" scope="request"/>
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<g:sortableColumn property="titulo" title="${message(code: 'bug.titulo.label', default: 'Titulo')}" params="${filterParams}" />
						<g:sortableColumn property="tipo" title="${message(code: 'bug.tipo.label', default: 'Tipo')}" params="${filterParams}" />
						<g:sortableColumn property="data" title="${message(code: 'bug.data.label', default: 'Data')}" params="${filterParams}" />
						<th><g:message code="bug.usuario.label" default="Usuario" /></th>
                        <g:sortableColumn property="idSituacao" title="${message(code: 'bug.situacao.label', default: 'Situação')}" params="${filterParams}" />
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${bugInstanceList}" status="i" var="bugInstance">
					<tr>
						
						<td><g:link action="show" id="${bugInstance.id}">${fieldValue(bean: bugInstance, field: "titulo")}</g:link></td>
						<td>${fieldValue(bean: bugInstance, field: "tipo")}</td>
						<td><g:formatDate date="${bugInstance.data}" /></td>
						<td>${bugInstance.usuario?.nomeCompleto}</td>
						<td><g:message code="${bugInstance.situacao?.label}" /></td>
						<td class="acoes">
							<g:if test="${!bugInstance.situacao?.fechado}">
								<will:editLink id="${bugInstance.id}" />
								<will:deleteConfirmButton id="${bugInstance.id}" />
							</g:if>
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${bugInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>