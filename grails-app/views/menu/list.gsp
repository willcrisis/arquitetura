
<%@ page import="arquitetura.i18n.I18n; arquitetura.menu.Menu" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'menu.plural.label', default: 'Menus')}" scope="request" />
		<g:set var="domainName" value="arquitetura.menu.Menu" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
				<tr>

					<g:sortableColumn property="label" title="${message(code: 'menu.label.label', default: 'Label')}" />

					<th><g:message code="menu.titulo.label" default="Titulo" /></th>

					<g:sortableColumn property="controlador" title="${message(code: 'menu.controlador.label', default: 'Controlador')}" />

					<g:sortableColumn property="acao" title="${message(code: 'menu.acao.label', default: 'Acao')}" />

					<th><g:message code="menu.pai.label" default="Pai" /></th>

					<th class="acoes"><g:message code="sistema.acoes.label"/></th>
				</tr>
				</thead>
				<tbody>
				<g:each in="${menuInstanceList}" status="i" var="menuInstance">
					<tr>

						<td><g:link action="show" id="${menuInstance.id}">${fieldValue(bean: menuInstance, field: "label")}</g:link></td>

						<g:set var="titulo" value="${arquitetura.i18n.I18n.findByKeyAndLanguage(menuInstance.label, will.i18nLocale())}" />
						<td>
							<g:message code="${menuInstance.label}" />
							<will:editI18nButton tituloPopup="menu.titulo.label" label="${menuInstance.label}" controllerRetorno="menu" id="${menuInstance.id}" />
						</td>

						<td>${fieldValue(bean: menuInstance, field: "controlador")}</td>

						<td>${fieldValue(bean: menuInstance, field: "acao")}</td>

						<td>${fieldValue(bean: menuInstance, field: "pai")}</td>
						
						<td class="acoes">
							<will:editLink id="${menuInstance.id}" />
							<will:deleteConfirmButton id="${menuInstance.id}" />
							<will:actionButton type="button" icone="${menuInstance.ativo ? 'eye' : 'eye-slash'}" data-toggle="modal" data-target="#alternar${menuInstance.id}" title="${message(code: "menu.${menuInstance.ativo ? 'desativar' : 'ativar'}.tooltip")}" />
							<will:confirmacao id="alternar${menuInstance.id}"
										   mensagem="${message(code: "menu.${menuInstance.ativo ? 'inativacao' : 'ativacao'}.confirmar.message")}"
										   linkConfirma="${createLink(action: 'alternar', id: menuInstance.id)}"/>
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${menuInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>