<%@ page import="arquitetura.menu.Menu" %>

<script type="text/javascript">
	jQuery(function ($) {
		mostrarOcultarCampos($("input[name='ehPai']"))
	})
	function mostrarOcultarCampos(campo) {
		$(".ocultar").toggle(!$(campo).is(':checked'))
//		if ($(campo).attr('checked')) {
//			$(".ocultar").hide()
//		} else {
//			$(".ocultar").show()
//		}
	}
</script>

<div class="form-group">
	<label for="label" class="col-lg-2 control-label">
		<g:message code="menu.label.label" default="Label" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="label" maxlength="200" required="" value="${menuInstance?.label}"/>
	</div>
</div>

<div class="form-group">
	<label for="icone" class="col-lg-2 control-label">
		<g:message code="menu.icone.label" default="Label" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4 iconpicker-container">
		<will:textField class="iconpicker" name="icone" maxlength="50" required="" value="${menuInstance?.icone}"/>
	</div>
</div>

<div class="form-group">
	<label for="label" class="col-lg-2 control-label">
		<g:message code="menu.ativo.label" default="Ativo" />
	</label>
	<div class="col-lg-4">
		<will:checkBox name="ativo" value="${menuInstance.ativo}" />
	</div>
</div>

<div class="form-group">
	<label for="label" class="col-lg-2 control-label">
		<g:message code="menu.ehPai.label" default="EhPai" />
	</label>
	<div class="col-lg-4">
		<will:checkBox name="ehPai" value="${menuInstance.ehPai}" onchange="mostrarOcultarCampos(this)" />
	</div>
</div>

<div class="form-group ocultar">
	<label for="controlador" class="col-lg-2 control-label">
		<g:message code="menu.controlador.label" default="Controlador" />

	</label>
	<div class="col-lg-4">
		<will:textField name="controlador" maxlength="200" value="${menuInstance?.controlador}"/>
	</div>
</div>

<div class="form-group ocultar">
	<label for="acao" class="col-lg-2 control-label">
		<g:message code="menu.acao.label" default="Acao" />

	</label>
	<div class="col-lg-4">
		<will:textField name="acao" maxlength="200" value="${menuInstance?.acao}"/>
	</div>
</div>

<div class="form-group">
	<label for="acao" class="col-lg-2 control-label">
		<g:message code="menu.ordem.label" default="Ordem" />

	</label>
	<div class="col-lg-4">
		<will:textField class="numero" name="ordem" maxlength="5" value="${menuInstance?.ordem}"/>
	</div>
</div>

<div class="form-group">
	<label for="pai" class="col-lg-2 control-label">
		<g:message code="menu.pai.label" default="Pai" />

	</label>
	<div class="col-lg-4">
		<will:select id="pai" name="pai.id" from="${arquitetura.menu.Menu.list()}" optionKey="id" value="${menuInstance?.pai?.id}" noSelection="['null': '']"/>
	</div>
</div>

<div class="form-group">
	<label for="permissoes" class="col-lg-2 control-label">
		<g:message code="menu.permissoes.label" default="Permissoes" />
	</label>
	<div class="col-lg-6">
		<will:select id="permissoes" name="permissoes" from="${arquitetura.seguranca.Papel.list()}" value="${menuInstance?.permissoes}" multiple="multiple" optionKey="id" optionValue="authority" />
	</div>
</div>