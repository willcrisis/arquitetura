
<%@ page import="arquitetura.menu.Menu" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'menu.plural.label', default: 'Menus')}" scope="request" />
	<g:set var="instancia" value="${menuInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.label.label" default="Label" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${menuInstance}" field="label"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.controlador.label" default="Controlador" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${menuInstance}" field="controlador"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.acao.label" default="Acao" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${menuInstance}" field="acao"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.pai.label" default="Pai" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:link controller="menu" action="show" id="${menuInstance?.pai?.id}">${menuInstance?.pai?.encodeAsHTML()}</g:link>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.ativo.label" default="Ativo" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:formatBoolean boolean="${menuInstance?.ativo}" />
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.ehPai.label" default="Eh Pai" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:formatBoolean boolean="${menuInstance?.ehPai}" />
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.ordem.label" default="Ordem" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${menuInstance}" field="ordem"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="menu.permissoes.label" default="Permissoes" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:each in="${menuInstance.permissoes}" var="p">
					<g:link controller="papel" action="show" id="${p.id}">${p?.authority?.encodeAsHTML()}</g:link><br />
				</g:each>
				
			</p>
		</div>
	</div>
	
</body>
</html>