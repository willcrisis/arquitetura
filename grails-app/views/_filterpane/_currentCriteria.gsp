<%@ page import="org.grails.plugin.filterpane.FilterPaneOperationType" %>
<g:if test="${isFiltered == true}">
    <div style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
        <g:each in="${criteria}" var="c">
            <label class="control-label">
                ${c.fieldName}
            </label>
            <g:message code="fp.op.${c.filterOp}" default="${c.filterOp}"/>
            <g:if test="${![FilterPaneOperationType.IsNull.operation, FilterPaneOperationType.IsNotNull.operation].contains(c.filterOp)}">
                <g:if test="${quoteValues == true}">
                    "${c.filterValue}"
                </g:if>
                <g:else>
                    ${c.filterValue}
                </g:else>
            </g:if>
            <g:if test="${'between'.equalsIgnoreCase(c.filterOp)}">
                <g:message code="fp.tag.filterPane.property.betweenValueSeparatorText" default="and"/>
                <g:if test="${quoteValues == true}">
                    "${c.filterValueTo}"
                </g:if>
                <g:else>
                    ${c.filterValueTo}
                </g:else>
            </g:if>
            <a href="${g.createLink(action: action, params: c.params)}" %{--class="btn btn-danger"--}% style="padding: 3px 6px"><i class="fa fa-fw fa-trash-o"></i></a>
        </g:each>
    </div>
</g:if>
