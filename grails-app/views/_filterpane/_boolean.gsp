<g:select name="${name}"
    from="${[true: message(code: 'fp.tag.filterPane.property.boolean.true', default: 'yes'), false: message(code: 'fp.tag.filterPane.property.boolean.false', default: 'no')]}"
    onchange="grailsFilterPane.selectDefaultOperator('${opName}')"
    optionKey="key"
    optionValue="value"
    value="${value}"
    noSelection="${['':'']}"
    class="form-control"
/>
