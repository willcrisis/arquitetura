<div class="form-group" style="padding-left: 10px">
        <will:actionButton type="button" icone="times" class="btn btn-flat btn-default" data-toggle="collapse" data-target="#filtros">${cancelText}</will:actionButton>
        <will:actionButton type="button" icone="repeat" class="btn btn-flat btn-warning" onclick="return grailsFilterPane.clearFilterPane('${formName}');">${clearText}</will:actionButton>
        <will:submitButton action="${action}" class="btn btn-flat btn-success">${applyText}</will:submitButton>
</div>
