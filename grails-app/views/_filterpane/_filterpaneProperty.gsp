<tr>
<td  style="width: 20%; padding-right: 10px;">
        <label class="col-sm-8 control-label">
            ${fieldLabel}
        </label>
</td>
<td style="width: 20%; padding-right: 10px;">
    <div class="form-group" style="width: 100%">
            <g:select id="${opName}"
                      name="${opName}"
                      from="${opKeys}"
                      keys="${opKeys}"
                      value="${opValue}"
                      valueMessagePrefix="fp.op"
                      class="form-control"
                      style="width: 100%"
                      onChange="grailsFilterPane.filterOpChange('${opName}', '${ctrlAttrs.id}');" />
        </div>
    </div>
</td>
<g:if test="${toCtrlAttrs == null}">
    <td style="width: 60%; padding-right: 10px;" colspan="2">
        <div class="form-group">
            <g:set var="ctrlAttrsHelper" value="${ctrlAttrs.put('class', 'form-control')}" />
            <g:set var="ctrlAttrsHelper" value="${ctrlAttrs.remove('multiple')}" />
            <filterpane:input ctrlType="${ctrlType}" ctrlAttrs="${ctrlAttrs}" />
        </div>
    </td>
</g:if><g:else>
    <td style="width: 30%; padding-right: 10px;">
        <div class="form-group">
            <g:set var="ctrlAttrsHelper" value="${ctrlAttrs.put('class', 'form-control')}" />
            <filterpane:input ctrlType="${ctrlType}" ctrlAttrs="${ctrlAttrs}" />
        </div>
    </td>
    <td style="width: 30%; padding-right: 10px;">
        <div class="form-group">
            <span style="${toCtrlSpanStyle}" id="between-span-${toCtrlAttrs.id}">
                <g:set var="toCtrlAttrsHelper" value="${toCtrlAttrs.put('class', 'form-control')}" />
                <filterpane:input ctrlType="${ctrlType}" ctrlAttrs="${toCtrlAttrs}" />
            </span>
        </div>
    </td>
</g:else>
</tr>
