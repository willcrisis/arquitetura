<div id="${id}_clone" style="display:none;">
    <g:if test="${readonly}">
        <g:render template="/oneToMany/${componente}/readonly" model="${[i: "_clone"]}" />
    </g:if><g:else>
        <g:render template="/oneToMany/${componente}/editavel" model="${[i: "_clone"]}" />
        <div class="form-group">
            <div class="col-lg-3">
                <button name="${id}Remover_clone" id="${id}Remover_clone" type="button" class="btn btn-danger" onclick="remover('${id}', '_clone')"><i class="fa fa-fw fa-trash-o"></i></button>
            </div>
        </div>
    </g:else>
</div>
<g:each var="item" in="${lista}" status="i">
    <div id="${id}${i}" <g:if test="${hidden}">style="display:none;"</g:if>>
        <g:if test="${readonly}">
            <g:render template="/oneToMany/${componente}/readonly" model="${[i: i, item: item]}" />
        </g:if><g:else>
            <g:render template="/oneToMany/${componente}/editavel" model="${[i: i, item: item]}" />
            <div class="form-group">
                <div class="col-lg-3">
                    <button name="${id}Remover${i}" id="${id}Remover${i}" type="button" class="btn btn-danger" onclick="remover('${id}', '${i}')"><i class="fa fa-fw fa-times"></i></button>
                </div>
            </div>
        </g:else>
    </div>
</g:each>