    <g:hiddenField name='enderecos[${i}].id' value='${item?.id}'/>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.logradouro.label" default="Logradouro" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <will:textField name='enderecos[${i}].logradouro' value='${item?.logradouro}' />
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.numero.label" default="Numero" />
        </label>
        <div class="col-lg-8">
            <will:textField name='enderecos[${i}].numero' value='${item?.numero}' />
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.bairro.label" default="Bairro" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <will:textField name='enderecos[${i}].bairro' value='${item?.bairro}' />
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.cep.label" default="Cep" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <will:textField name='enderecos[${i}].cep' class="cep" value='${item?.cep}' />
        </div>
    </div>
