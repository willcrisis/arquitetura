    <g:hiddenField name='enderecos[${i}].id' value='${item?.id}'/>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.logradouro.label" default="Logradouro" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <p class="form-control-static">${item?.logradouro}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.numero.label" default="Numero" />
        </label>
        <div class="col-lg-8">
            <p class="form-control-static">${item?.numero}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.bairro.label" default="Bairro" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <p class="form-control-static">${item?.bairro}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">
            <g:message code="endereco.cep.label" default="Cep" />
            <span class="required-indicator">*</span>
        </label>
        <div class="col-lg-8">
            <p class="form-control-static">${item?.cep}</p>
        </div>
    </div>
