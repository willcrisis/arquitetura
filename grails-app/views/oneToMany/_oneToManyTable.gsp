<tr id="${id}_clone" class="clone" style="display: none">
    <g:if test="${readonly}">
        <g:render template="/oneToMany/${componente}/readonly" model="${[i: "_clone"]}" />
    </g:if><g:else>
        <g:render template="/oneToMany/${componente}/editavel" model="${[i: "_clone"]}" />
        <td class="acoes">
            <button name="${id}AdicionarAcima_clone" id="${id}AdicionarAcima_clone" type="button" class="btn btn-flat btn-default" onclick="adicionarAcima('${id}', $(this).parent().parent().index())" title="${message(code: 'default.oneToMany.adicionarAcima.label')}"><i class="fa fa-fw fa-level-up"></i></button>
            <button name="${id}AdicionarAbaixo_clone" id="${id}AdicionarAbaixo_clone" type="button" class="btn btn-flat btn-default" onclick="adicionarAbaixo('${id}', $(this).parent().parent().index())" title="${message(code: 'default.oneToMany.adicionarAbaixo.label')}"><i class="fa fa-fw fa-level-down"></i></button>
            <button name="${id}Remover_clone" id="${id}Remover_clone" type="button" class="btn btn-flat btn-danger" onclick="removerTabela('${id}', $(this).parent().parent().index())"><i class="fa fa-fw fa-times"></i></button>
        </td>
    </g:else>
</tr>

<g:each var="item" in="${lista}" status="i">
    <tr id="${id}${i}">
        <g:if test="${readonly}">
            <g:render template="/oneToMany/${componente}/readonly" model="${[i: i, item: item]}" />
        </g:if><g:else>
            <g:render template="/oneToMany/${componente}/editavel" model="${[i: i, item: item]}" />
            <td class="acoes">
                <button name="${id}AdicionarAcima${i}" id="${id}AdicionarAcima${i}" type="button" class="btn btn-flat btn-default" onclick="adicionarAcima('${id}', $(this).parent().parent().index())" title="${message(code: 'default.oneToMany.adicionarAcima.label')}"><i class="fa fa-fw fa-level-up"></i></button>
                <button name="${id}AdicionarAbaixo${i}" id="${id}AdicionarAbaixo${i}" type="button" class="btn btn-flat btn-default" onclick="adicionarAbaixo('${id}', $(this).parent().parent().index())" title="${message(code: 'default.oneToMany.adicionarAbaixo.label')}"><i class="fa fa-fw fa-level-down"></i></button>
                <button name="${id}Remover${i}" id="${id}Remover${i}" type="button" class="btn btn-flat btn-danger" onclick="removerTabela('${id}', $(this).parent().parent().index())"><i class="fa fa-fw fa-times"></i></button>
            </td>
        </g:else>
    </tr>
</g:each>