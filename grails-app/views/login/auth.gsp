<html>
<head>
	<meta name='layout' content='login'/>
</head>

<body>
<g:render template="/layouts/messages" />
<div class="container">
    <form action='${postUrl}' method='POST' id='loginForm' class="form-signin" autocomplete='off'>
        <h2 class="form-signin-heading">
            <g:message code="springSecurity.login.header"/>
        </h2>
			<p>
				<input type='text' class="form-control" name='j_username' id='username' placeholder="${message(code: "springSecurity.login.username.label")}" />
			</p>

			<p>
				<input type='password' class="form-control" name='j_password' id='password' placeholder="${message(code:"springSecurity.login.password.label")}" />
			</p>

			<p id="remember_me_holder">
                <label class="checkbox">
                    <input type="checkbox" value="remember-me" name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if> /> <g:message code="springSecurity.login.remember.me.label"/>
                </label>
			</p>

			<p>
				<button class="btn btn-lg btn-success btn-block" type='submit' id="submit">${message(code: "springSecurity.login.button")}</button>
			</p>

			<p>
				<will:linkButton action="registro" controller="usuario" class="btn btn-lg btn-primary btn-block" title="${message(code: 'default.registrar.label')}"><g:message code="default.registrar.label"/></will:linkButton>
			</p>
		</form>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
