<%@ page import="arquitetura.seguranca.Papel" %>



<div class="form-group">
	<label for="authority" class="col-lg-2 control-label">
		<g:message code="papel.authority.label" default="Authority"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="authority" required="" value="${papelInstance?.authority}"/>

	</div>
</div>

