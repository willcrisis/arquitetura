<%@ page import="arquitetura.seguranca.Papel" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'papel.label', default: 'Papel')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'papel.plural.label', default: 'Papel')}" scope="request" />
		<g:set var="instancia" value="${papelInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:render template="form"/>
	</body>
</html>