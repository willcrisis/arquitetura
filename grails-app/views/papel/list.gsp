
<%@ page import="arquitetura.seguranca.Papel" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'papel.label', default: 'Papel')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'papel.plural.label', default: 'Papels')}" scope="request" />
		<g:set var="domainName" value="arquitetura.seguranca.Papel" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						
						<g:sortableColumn property="authority" title="${message(code: 'papel.authority.label', default: 'Authority')}" />
						
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${papelInstanceList}" status="i" var="papelInstance">
					<tr>
						
						<td><g:link action="show" id="${papelInstance.id}">${fieldValue(bean: papelInstance, field: "authority")}</g:link></td>
						
						<td class="acoes">
							<will:editLink id="${papelInstance.id}" />
							<will:deleteConfirmButton id="${papelInstance.id}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${papelInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>