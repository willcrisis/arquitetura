
<%@ page import="arquitetura.seguranca.Papel" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'papel.label', default: 'Papel')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'papel.plural.label', default: 'Papels')}" scope="request" />
	<g:set var="instancia" value="${papelInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="papel.authority.label" default="Authority" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${papelInstance}" field="authority"/>
				
			</p>
		</div>
	</div>
	
</body>
</html>