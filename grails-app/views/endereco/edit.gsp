<%@ page import="arquitetura.seguranca.Endereco" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="edit">
		<g:set var="entityName" value="${message(code: 'endereco.label', default: 'Endereco')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'endereco.plural.label', default: 'Endereco')}" scope="request" />
		<g:set var="instancia" value="${enderecoInstance}" scope="request" />
		<title><g:message code="default.edit.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:hiddenField name="id" value="${enderecoInstance?.id}" />
		<g:hiddenField name="versao" value="${enderecoInstance?.versao}" />
		<g:render template="form"/>
	</body>
</html>