<%@ page import="arquitetura.seguranca.Endereco" %>



<div class="form-group">
	<label for="url" class="col-lg-2 control-label">
		<g:message code="endereco.url.label" default="Url"/>
		<span class="required-indicator">*</span>
	</label>

	<div class="col-lg-4">
		<will:textField name="url" required="" value="${enderecoInstance?.url}"/>
	</div>
</div>

<div class="form-group">
	<label for="configAttribute" class="col-lg-2 control-label">
		<g:message code="endereco.configAttribute.label" default="Config Attribute"/>
		<span class="required-indicator">*</span>
	</label>

	<div class="col-lg-6">
		<will:select from="${permissoes}" id="configAttribute" name="configAttribute" value="${enderecoInstance.configAttribute}" multiple="multiple" />
	</div>
</div>