
<%@ page import="arquitetura.seguranca.Endereco" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'endereco.label', default: 'Endereco')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'endereco.plural.label', default: 'Enderecos')}" scope="request" />
	<g:set var="instancia" value="${enderecoInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="endereco.url.label" default="Url" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${enderecoInstance}" field="url"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="endereco.configAttribute.label" default="Config Attribute" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${enderecoInstance}" field="configAttribute"/>
				
			</p>
		</div>
	</div>
	
</body>
</html>