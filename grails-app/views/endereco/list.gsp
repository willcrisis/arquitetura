
<%@ page import="arquitetura.seguranca.Endereco" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'endereco.label', default: 'Endereco')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'endereco.plural.label', default: 'Enderecos')}" scope="request" />
		<g:set var="domainName" value="arquitetura.seguranca.Endereco" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						
						<g:sortableColumn property="url" title="${message(code: 'endereco.url.label', default: 'Url')}" />
						
						<g:sortableColumn property="configAttribute" title="${message(code: 'endereco.configAttribute.label', default: 'Config Attribute')}" />
						
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${enderecoInstanceList}" status="i" var="enderecoInstance">
					<tr>
						
						<td><g:link action="show" id="${enderecoInstance.id}">${fieldValue(bean: enderecoInstance, field: "url")}</g:link></td>
						
						<td>${fieldValue(bean: enderecoInstance, field: "configAttribute")}</td>
						
						<td class="acoes">
							<will:editLink id="${enderecoInstance.id}" />
							<will:deleteConfirmButton id="${enderecoInstance.id}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${enderecoInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>