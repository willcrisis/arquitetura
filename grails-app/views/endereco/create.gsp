<%@ page import="arquitetura.seguranca.Endereco" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'endereco.label', default: 'Endereco')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'endereco.plural.label', default: 'Endereco')}" scope="request" />
		<g:set var="instancia" value="${enderecoInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
	    <g:render template="form"/>
	</body>
</html>