
<%@ page import="arquitetura.i18n.I18n" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'i18n.label', default: 'I18n')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'i18n.plural.label', default: 'I18ns')}" scope="request" />
	<g:set var="instancia" value="${i18nInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="i18n.key.label" default="Key" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${i18nInstance}" field="key"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="i18n.language.label" default="Language" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${i18nInstance}" field="language"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="i18n.value.label" default="Value" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${i18nInstance}" field="value"/>
				
			</p>
		</div>
	</div>
	
</body>
</html>