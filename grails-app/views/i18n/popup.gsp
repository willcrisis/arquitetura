<%@ page import="arquitetura.i18n.I18n" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="popup">
		<g:set var="entityName" value="${message(code: 'i18n.label', default: 'I18n')}" />
		<g:set var="entityNamePlural" value="${message(code: 'i18n.plural.label', default: 'I18ns')}" />
        <g:set var="instancia" value="${i18nInstance}" />
		<title><g:message code="default.new.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-menu" class="container" role="main">
			<g:form action="salvarPopup" class="form-horizontal" role="form" >
                <g:hiddenField name="id" value="${i18nInstance?.id}" />
                <g:hiddenField name="version" value="${i18nInstance?.version}" />
			    <g:render template="form"/>

                <div class="action-buttons">
                    <will:submitButtonFlutuante action="salvarPopup" />
                </div>
			</g:form>
		</div>
	</body>
</html>
