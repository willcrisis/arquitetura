
<%@ page import="arquitetura.i18n.I18n" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'i18n.label', default: 'I18n')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'i18n.plural.label', default: 'I18ns')}" scope="request" />
		<g:set var="domainName" value="arquitetura.i18n.I18n" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						
						<g:sortableColumn property="key" title="${message(code: 'i18n.key.label', default: 'Key')}" params="${filterParams}" />
						
						<g:sortableColumn property="language" title="${message(code: 'i18n.language.label', default: 'Language')}" params="${filterParams}" />
						
						<g:sortableColumn property="value" title="${message(code: 'i18n.value.label', default: 'Value')}" params="${filterParams}" />
						
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${i18nInstanceList}" status="i" var="i18nInstance">
					<tr>
						
						<td><g:link action="show" id="${i18nInstance.id}">${fieldValue(bean: i18nInstance, field: "key")}</g:link></td>
						
						<td>${fieldValue(bean: i18nInstance, field: "language")}</td>
						
						<td>${fieldValue(bean: i18nInstance, field: "value")}</td>
						
						<td class="acoes">
							<will:editLink id="${i18nInstance.id}" />
							<will:deleteConfirmButton id="${i18nInstance.id}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${i18nInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>