<%@ page import="arquitetura.i18n.I18n" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'i18n.label', default: 'I18n')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'i18n.plural.label', default: 'I18n')}" scope="request" />
		<g:set var="instancia" value="${i18nInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
        <g:render template="form"/>
	</body>
</html>