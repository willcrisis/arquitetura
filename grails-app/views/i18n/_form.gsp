<%@ page import="arquitetura.i18n.I18n" %>



<div class="form-group">
	<label for="key" class="col-lg-2 control-label">
		<g:message code="i18n.key.label" default="Key"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="key" maxlength="200" required="" value="${i18nInstance?.key}"/>

	</div>
</div>

<div class="form-group">
	<label for="language" class="col-lg-2 control-label">
		<g:message code="i18n.language.label" default="Language"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:localeSelect name="language" value="${i18nInstance?.language}"  />

	</div>
</div>

<div class="form-group">
	<label for="value" class="col-lg-2 control-label">
		<g:message code="i18n.value.label" default="Value"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="value" maxlength="200" required="" value="${i18nInstance?.value}"/>

	</div>
</div>

