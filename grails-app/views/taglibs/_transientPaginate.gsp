<div class="pagination">
    <ul class="pagination" id="paginate-${tableId}">

    </ul>
</div>

<script type="text/javascript">

    var ${tableId}PaginaAtual = 1;
    var ${tableId}Max;
    var ${tableId}UltimaPagina;

    $(document).ready(function () {
        ${tableId}IniciarPaginacao(${max});
    });

    function ${tableId}IniciarPaginacao(max) {
        ${tableId}Max = max;

        var paginacao = $('#paginate-${tableId}');
        paginacao.empty();

        var linhas = $('#' + '${tableId} tbody tr[class!="clone"]');

        var minHeight = ($(linhas[0]).height() * ${tableId}Max) + 21;
        $('#'+ '${tableId}').wrap('<div class="table-wrap" style="min-height: ' + minHeight + 'px"></div>"');

        var total = linhas.length;
        var paginas = Math.ceil(total / ${tableId}Max);

        ${tableId}UltimaPagina = paginas;

        paginacao.append('<li id="primeira-${tableId}" class="prev" style="cursor: pointer" onclick="${tableId}IrParaPagina(1)"><span><g:message code="paginate.first" default="&laquo;" /> </span></li>');
        if (${tableId}PaginaAtual == 1) {
            paginacao.append('<li id="anterior-${tableId}" class="prev disabled" style="cursor: pointer"><span><g:message code="paginate.prev" default="&lsaquo;" /> </span></li>');
        } else {
            paginacao.append('<li id="anterior-${tableId}" class="prev" style="cursor: pointer" onclick="paginaAnterior${tableId}()"><span><g:message code="paginate.prev" default="&lsaquo;" /> </span></li>');
        }
        for (var i = 1; i <= paginas; i++) {
            paginacao.append(criarNumeroPagina${tableId}(i, ${tableId}PaginaAtual == i));
        }
        if (paginas > 1 && ${tableId}PaginaAtual != paginas) {
            paginacao.append('<li id="proximo-${tableId}" class="next" style="cursor: pointer" onclick="proximaPagina${tableId}()"><span><g:message code="paginate.next" default="&rsaquo;" /></span></li>');
        } else {
            paginacao.append('<li id="proximo-${tableId}" class="next disabled" style="cursor: pointer"><span><g:message code="paginate.next" default="&rsaquo;" /></span></li>');
        }
        paginacao.append('<li id="ultima-${tableId}" class="next" style="cursor: pointer" onclick="${tableId}IrParaPagina(' + paginas + ')"><span><g:message code="paginate.last" default="&raquo;" /> </span></li>');

        exibirEOcultarLinhas${tableId}(linhas, ${tableId}PaginaAtual);
    }

    function criarNumeroPagina${tableId}(pagina, ativo) {
        return '<li' + (ativo ? ' class="active"' : '') + ' style="cursor: pointer" id="pagina-' + pagina + '-${tableId}" onclick="${tableId}IrParaPagina(' + pagina + ')"><span>' + pagina + '</span></li>'
    }

    function proximaPagina${tableId}() {
        ${tableId}IrParaPagina(${tableId}PaginaAtual + 1);
    }

    function paginaAnterior${tableId}() {
        ${tableId}IrParaPagina(${tableId}PaginaAtual - 1);
    }

    function exibirEOcultarLinhas${tableId}(linhas, pagina) {
        var fim = pagina * ${tableId}Max;
        var inicio = fim - ${tableId}Max;
        var total = linhas.length;

        for (var i = inicio; i < fim; i++) {
            linhas.eq(i).show();
        }
        for (var i = 0; i < inicio; i++) {
            linhas.eq(i).hide();
        }
        for (var i = fim; i < total; i++) {
            linhas.eq(i).hide();
        }

        exibirEOcultarPaginas${tableId}(pagina);
    }

    function exibirEOcultarPaginas${tableId}(pagina) {
        if (${tableId}UltimaPagina > 5) {
            if (pagina <= 3) {
                for (var i = 1; i <= 5; i++) {
                    $('#pagina-' + i + '-${tableId}').show();
                }
                for (var i = 6; i <= ${tableId}UltimaPagina; i++) {
                    $('#pagina-' + i + '-${tableId}').hide();
                }
            } else if (pagina >= (${tableId}UltimaPagina - 2)) {
                for (var i = 1; i <= (${tableId}UltimaPagina - 5); i++) {
                    $('#pagina-' + i + '-${tableId}').hide();
                }
                for (var i = (${tableId}UltimaPagina - 4); i <= ${tableId}UltimaPagina; i++) {
                    $('#pagina-' + i + '-${tableId}').show();
                }
            } else {
                for (var i = 1; i <= (pagina - 3); i++) {
                    $('#pagina-' + i + '-${tableId}').hide();
                }
                for (var i = (pagina - 2); i <= (pagina + 2); i++) {
                    $('#pagina-' + i + '-${tableId}').show();
                }
                for (var i = (pagina + 3); i <= ${tableId}UltimaPagina; i++) {
                    $('#pagina-' + i + '-${tableId}').hide();
                }
            }
        }
    }

    function ${tableId}IrParaPagina(pagina) {
        var linhas = $('#' + '${tableId} tbody tr[class!="clone"]');
        var total = linhas.length;
        var paginas = Math.ceil(total / ${tableId}Max);

        exibirEOcultarLinhas${tableId}(linhas, pagina);

        $('#paginate-${tableId} li[class="active"]').removeAttr('class');
        $('#pagina-' + pagina + '-${tableId}').attr('class', 'active');

        var prev = $('#anterior-${tableId}');
        if (pagina == 1) {
            prev.attr('class', 'prev disabled');
            prev.removeAttr('onclick');
        } else {
            prev.attr('class', 'prev');
            prev.attr('onclick', 'paginaAnterior${tableId}()');
        }

        var next = $('#proximo-${tableId}');
        if (pagina == paginas) {
            next.attr('class', 'next disabled');
            next.removeAttr('onclick');
        } else {
            next.attr('class', 'next');
            next.attr('onclick', 'proximaPagina${tableId}()');
        }

        ${tableId}PaginaAtual = pagina;
    }
</script>