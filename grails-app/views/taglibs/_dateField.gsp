<div class="input-append ${tipo}">
    <div class="input-group">
        <input type="text" <g:if test="${tipo == 'dateTime'}">data-format="dd/MM/yyyy hh:mm"</g:if><g:else>data-format="dd/MM/yyyy"</g:else> class="form-control ${classe ?: ""}" name="${name}" ${required != null ? 'required=""' : ""} ${onlycalendar != null ? 'readonly=""' : ""} value="${value ?: ""}" style="background-color: #fff; color: #555; ${style ?: ""}" id="${id ?: name}">
        <span class="input-group-addon add-on"><i class="fa fa-fw fa-calendar" data-time-icon="fa fa-fw fa-clock-o" data-date-icon="fa fa-fw fa-calendar"></i></span>
    </div>
</div>