<div class="modal fade" id="${id ?: 'modal'}" tabindex="-1" role="dialog" aria-labelledby="${id ?: 'modal'}Label" aria-hidden="true">
    <div class="modal-dialog ${tamanho == 'lg' ? 'modal-lg' : tamanho == 'sm' ? 'modal-sm' : ''}">
        <div class="modal-content">
            <g:if test="${titulo}">
                <div class="modal-header">
                    %{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}%
                    <h3 class="modal-title" id="${id ?: 'modal'}Label">${titulo}</h3>
                </div>
            </g:if><g:elseif test="${confirmacao}">
                <div class="modal-header">
                    %{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}%
                    <h3 class="modal-title" id="${id ?: 'modal'}Label">${message(code: 'sistema.confirmacao.label')}</h3>
                </div>
            </g:elseif>
            <div class="modal-body">
                <g:if test="${mensagem || confirmacao}">
                    ${corpo.encodeAsRaw()}
                </g:if><g:elseif test="${iframe}">
                    <iframe id="frame-${id ?: 'modal'}" src="${link}"
                            style="width: 100%; height: ${altura}px; border: 0;"></iframe>
                </g:elseif>
            </div>
            <g:if test="${confirmacao}">
                <div class="modal-footer">
                    <will:actionButton class="btn btn-default" data-dismiss="modal">${textoCancela ?: message(code: 'default.boolean.false')}</will:actionButton>
                    <g:if test="${linkConfirma}">
                        <will:actionButton class="btn btn-primary" onclick="window.location = '${linkConfirma}'">${textoConfirma ?: message(code: 'default.boolean.true')}</will:actionButton>
                    </g:if><g:else>
                        <will:submitButton action="${action}">${textoConfirma ?: message(code: 'default.boolean.true')}</will:submitButton>
                    </g:else>
                </div>
            </g:if>
            <g:elseif test="${mensagem || iframe}">
                <div class="modal-footer">
                    <g:if test="${linkOk}">
                        <will:actionButton class="btn btn-primary" onclick="window.location = '${linkOk}'">${textoOk ?: message(code: 'sistema.botao.fechar')}</will:actionButton>
                    </g:if><g:else>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">${textoOk ?: message(code: 'sistema.botao.fechar')}</button>
                    </g:else>
                </div>
            </g:elseif>
            <g:elseif test="${temRodape}">
                <div class="modal-footer">
                    ${rodape}
                </div>
            </g:elseif>
        </div>
    </div>
</div>