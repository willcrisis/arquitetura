<%@ page import="arquitetura.bug.SituacaoBug" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="create">
		<g:set var="entityName" value="${message(code: 'situacaoBug.label', default: 'SituacaoBug')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'situacaoBug.plural.label', default: 'SituacaoBug')}" scope="request" />
		<g:set var="instancia" value="${situacaoBugInstance}" scope="request" />
		<title><g:message code="default.create.label" args="[entityName]"/></title>
	</head>

	<body>
		<g:render template="form"/>
	</body>
</html>