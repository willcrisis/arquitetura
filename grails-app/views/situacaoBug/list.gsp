
<%@ page import="arquitetura.bug.SituacaoBug" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="list">
		<g:set var="entityName" value="${message(code: 'situacaoBug.label', default: 'SituacaoBug')}" scope="request" />
		<g:set var="entityNamePlural" value="${message(code: 'situacaoBug.plural.label', default: 'SituacaoBugs')}" scope="request" />
		<g:set var="domainName" value="arquitetura.bug.SituacaoBug" scope="request" />
		%{--<g:set var="filterProperties" value=""/>--}%
		<title><g:message code="default.list.label" args="[entityNamePlural]" /></title>
	</head>

	<body>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						
						<g:sortableColumn property="label" title="${message(code: 'situacaoBug.label.label', default: 'Label')}" />
						
						<g:sortableColumn property="fechado" title="${message(code: 'situacaoBug.fechado.label', default: 'Fechado')}" />
						
						<th class="acoes"><g:message code="sistema.acoes.label" /> </th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${situacaoBugInstanceList}" status="i" var="situacaoBugInstance">
					<tr>
						
						<td>
							<g:link action="show" id="${situacaoBugInstance.id}"><g:message code="${situacaoBugInstance.label}" /></g:link>
							<will:editI18nButton tituloPopup="situacaoBug.titulo.label" label="${situacaoBugInstance.label}" controllerRetorno="situacaoBug" id="${situacaoBugInstance.id}" />
						</td>
						
						<td><g:formatBoolean boolean="${situacaoBugInstance.fechado}" /></td>
						
						<td class="acoes">
							<will:editLink id="${situacaoBugInstance.id}" />
							<will:deleteConfirmButton id="${situacaoBugInstance.id}" />
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>

		<div class="pagination">
			<g:paginate total="${situacaoBugInstanceCount}" params="${filterParams}"/>
		</div>
	</body>
</html>