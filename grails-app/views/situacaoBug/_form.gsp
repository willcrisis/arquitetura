<%@ page import="arquitetura.bug.SituacaoBug" %>



<div class="form-group">
	<label for="label" class="col-lg-2 control-label">
		<g:message code="situacaoBug.label.label" default="Label"/>
		<span class="required-indicator">*</span>
	</label>
	<div class="col-lg-4">
		<will:textField name="label" maxlength="200" required="" value="${situacaoBugInstance?.label}"/>

	</div>
</div>

<div class="form-group">
	<label for="fechado" class="col-lg-2 control-label">
		<g:message code="situacaoBug.fechado.label" default="Fechado"/>
		
	</label>
	<div class="col-lg-4">
		<will:checkBox name="fechado" value="${situacaoBugInstance?.fechado}" />

	</div>
</div>

