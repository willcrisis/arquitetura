
<%@ page import="arquitetura.bug.SituacaoBug" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="show">
	<g:set var="entityName" value="${message(code: 'situacaoBug.label', default: 'SituacaoBug')}" scope="request" />
	<g:set var="entityNamePlural" value="${message(code: 'situacaoBug.plural.label', default: 'SituacaoBugs')}" scope="request" />
	<g:set var="instancia" value="${situacaoBugInstance}" scope="request" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="situacaoBug.label.label" default="Label" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:fieldValue bean="${situacaoBugInstance}" field="label"/>
				
			</p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-lg-2 control-label">
			<g:message code="situacaoBug.fechado.label" default="Fechado" />
		</label>
		<div class="col-lg-4">
			<p class="form-control-static">
				
				<g:formatBoolean boolean="${situacaoBugInstance?.fechado}" />
				
			</p>
		</div>
	</div>
	
</body>
</html>