package arquitetura

import org.springframework.web.servlet.support.RequestContextUtils

class I18nTagLib {

    static namespace = "will"

    def i18nService

    def i18nLocale = { attrs, body ->
        out << obterLocale()
    }

    def editI18nButton = { attrs ->
        Map model = [:]
        model.id = attrs.id
        model.tituloPopup = attrs.tituloPopup ?: 'i18n.label'
        model.controllerRetorno = attrs.controllerRetorno
        model.actionRetorno = attrs.actionRetorno ?: 'index'
        model.label = attrs.label
        model.i18n = i18nService.consultar(model.label, obterLocale())

        out << render(template: '/taglibs/editI18nButton', plugin: 'arquitetura', model: model)
    }

    protected def obterLocale() {
        RequestContextUtils.getLocale(request)
    }
}
