package arquitetura

class TransientPaginateTagLib {

    static namespace = 'will'

    def transientPaginate = { attrs ->
        def tableId = attrs.remove('tableId')
        def max = attrs.remove('max')
        max = max ?: 10
        out << render(template: '/taglibs/transientPaginate', model: [tableId: tableId, max: max], plugin: 'arquitetura')
    }
}
