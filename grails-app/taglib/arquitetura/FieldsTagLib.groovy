package arquitetura

import utils.TratadorDeParametros

class FieldsTagLib {
    static namespace = 'will'

    def textField = { attrs, body ->
        attrs.class += ' form-control'
        out << g.textField(attrs, body)
    }

    def textArea = { attrs, body ->
        out << '<div>'
        attrs.class += ' form-control'
        def maxlength = TratadorDeParametros.somenteNumerosInteger(attrs.maxlength)
        String value = attrs.value
        out << g.textArea(attrs, body)
        if (maxlength) {
            out << "<p>${message(code: 'default.caracteresRestantes.label')}<span>${value ? maxlength - value.size() : maxlength}</span></p>"
        }
        out << '</div>'
    }

    def select = { attrs, body ->
        attrs.class += ' form-control'
        out << g.select(attrs, body)
    }

    def localeSelect = { attrs, body ->
        attrs.class += ' form-control'
        out << g.localeSelect(attrs, body)
    }

    def timeZoneSelect = { attrs, body ->
        attrs.class += ' form-control'
        out << g.timeZoneSelect(attrs, body)
    }

    def currencySelect = { attrs, body ->
        attrs.class += ' form-control'
        out << g.currencySelect(attrs, body)
    }

    def field = { attrs, body ->
        attrs.class += ' form-control'
        out << g.field(attrs, body)
    }

    def checkBox = { attrs, body ->
        out << '<div class="togglebutton">'
        out << '<label>'
        out << g.checkBox(attrs, body)
        out << " ${attrs.label ?: ''}"
        out << '</label>'
        out << '</div>'
    }

    def uploadField = { attrs, body ->
        def name = attrs.remove('name')
        def id = attrs.remove('id')
        id = id ?: name
        def extensoes = attrs.remove('extensoes')
        boolean required = Boolean.valueOf(attrs.remove('required'))
        out << render(template: '/taglibs/uploadField', model: [name: name, id: id, extensoes: extensoes, required: required], plugin: 'arquitetura')
    }

    def passwordField = { attrs, body ->
        attrs.class += ' form-control'
        out << g.passwordField(attrs, body)
    }
}
