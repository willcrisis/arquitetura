package arquitetura

class DateFieldTagLib {
    static namespace = 'will'

    def dateField = { attrs, body ->
        attrs.tipo = 'date'
        out << render(template: '/taglibs/dateField', model: attrs, plugin: 'arquitetura')
    }

    def dateTimeField = { attrs, body ->
        attrs.tipo = 'dateTime'
        out << render(template: '/taglibs/dateField', model: attrs, plugin: 'arquitetura')
    }
}
