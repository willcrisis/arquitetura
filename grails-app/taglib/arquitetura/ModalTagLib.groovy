package arquitetura

class ModalTagLib {
    static namespace = 'will'

    def modal = { attrs, body ->
        out << render(template: '/taglibs/modal', model: attrs, plugin: 'arquitetura')
    }

    def confirmacao = { attrs, body ->
        out << render(template: '/taglibs/modal', model: [
                id: attrs.id,
                titulo: attrs.titulo,
                corpo: attrs.mensagem,
                confirmacao: true,
                textoCancela: attrs.textoCancela,
                textoConfirma: attrs.textoConfirma,
                linkConfirma: attrs.linkConfirma,
                action: attrs.action
        ], plugin: 'arquitetura')
    }

    def mensagem = { attrs, body ->
        out << render(template: '/taglibs/modal', model: [
                id: attrs.id,
                titulo: attrs.titulo,
                corpo: attrs.mensagem ?: body(),
                mensagem: true,
                textoOk: attrs.textoOk,
                linkOk: attrs.linkOk,
                tamanho: attrs.tamanho
        ], plugin: 'arquitetura')
    }

    def iframe = { attrs, body ->
        attrs.class = attrs.class + ' btn btn-default'
        out << actionButton(
                onclick: "recarregarIframe('frame-${attrs.id}')",
                type: 'button',
                icone: attrs.icone,
                class: attrs.class,
                'data-toggle': 'modal',
                'data-target': "#${attrs.id}",
                title: attrs.titulo,
                body
        )
        out << render(template: '/taglibs/modal', model: [
                id: attrs.id,
                titulo: attrs.titulo,
                link: attrs.link,
                iframe: true,
                textoOk: attrs.textoOk,
                linkOk: attrs.linkOk,
                tamanho: attrs.tamanho,
                altura: attrs.altura ?: '370'
        ], plugin: 'arquitetura')
    }
}
