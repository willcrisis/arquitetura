package arquitetura

class ButtonsTagLib {
    static namespace = 'will'

    /**
     * Renderiza um botão de submissão de formulário
     *
     * @attr action REQUIRED Action a ser executada
     */
    def submitButton = {attrs, body ->
        attrs.tagName = 'submitButton'
        attrs.class = attrs.class ?: 'btn btn-success'
        attrs.icone = attrs.icone ?: 'check'

        out << processarBotao('submit', attrs.remove('icone') as String, attrs.remove('action') as String, attrs, body)
    }

    /**
     * @attr id REQUIRED ID do objeto a ser alterado
     */
    def editButton = {attrs, body ->
        attrs.tagName = 'editButton'
        attrs.class = 'btn btn-flat btn-info'
        attrs.action = attrs.action ?: 'edit'

        out << processarBotao('submit', 'pencil', attrs.remove('action') as String, attrs, body)
    }

    /**
     * @attr id REQUIRED ID do objeto a ser alterado
     */
    def editButtonFlutuante = {attrs, body ->
        attrs.tagName = 'editButton'
        attrs.class = 'btn btn-info btn-fab btn-raised'
        attrs.action = attrs.action ?: 'edit'

        out << processarBotao('submit', 'pencil', attrs.remove('action') as String, attrs, body)
    }

    def resetButton = {attrs, body ->
        attrs.tagName = 'resetButton'
        attrs.class = attrs.class ?: 'btn btn-flat btn-warning'

        out << processarBotao('reset', 'repeat', 'reset', attrs, body)
    }

    def actionButton = { attrs, body ->
        attrs.tagName = "actionButton"
        attrs.class = attrs.class ?: "btn btn-flat btn-primary"

        String value = attrs.remove('value')
        String action = attrs.remove('action') ?: value

        out << processarBotao('button', attrs.remove('icone') as String, action, attrs, body)
    }

    /**
     * @attr action REQUIRED Action do controlador a ser chamada
     */
    def linkButton = { attrs, body ->
        attrs.class = attrs.class ?: "btn btn-flat btn-default"
        String icone = attrs.icone ? "fa fa-fw fa-${attrs.icone ?: 'none'}" : ''

        out << "<a href=\"${createLink(action: attrs.remove('action'), id: attrs.remove("id"), controller: attrs.remove("controller"), params: attrs.remove('params'))}\" class=\"${attrs.class}\" title=\"${attrs.title}\">"
        out << "<i class=\"${icone}\"></i>"
        out << body()
        out << "</a>"
    }

    /**
     * @attr id REQUIRED ID do objeto a ser alterado
     */
    def editLink = { attrs, body ->
        attrs.action = attrs.action ?: 'edit'
        out << linkButton(class: attrs.class ?: 'btn btn-flat btn-default', id: attrs.remove("id"), action: attrs.remove("action"), controller: attrs.remove("controller"), title: "${message(code: 'default.button.edit.label', default: 'Edit')}", icone: 'pencil', body)
    }

    /**
     * @attr id REQUIRED ID do objeto a ser excluído
     */
    def deleteConfirmButton = { attrs, body ->
        out << actionButton(icone: 'trash-o', class: 'btn btn-flat btn-danger', 'data-toggle': 'modal', 'data-target': "#remover${attrs.id}", title: "${message(code: 'default.button.delete.label', default: 'Remove')}", body)
        out << confirmacao(id: "remover${attrs.id}", mensagem: "${message(code: 'default.button.delete.confirm.message')}", linkConfirma: "${createLink(action: 'delete', id: attrs.id)}")
    }

    /**
     * @attr id REQUIRED ID do objeto a ser excluído
     */
    def deleteConfirmButtonFlutuante = { attrs, body ->
        out << actionButtonFlutuante(icone: 'trash-o', tipo: 'danger', 'data-toggle': 'modal', 'data-target': "#remover${attrs.id}", title: "${message(code: 'default.button.delete.label', default: 'Remove')}", body)
        out << confirmacao(id: "remover${attrs.id}", mensagem: "${message(code: 'default.button.delete.confirm.message')}", linkConfirma: "${createLink(action: 'delete', id: attrs.id)}")
    }

    def linkFlutuante = { attrs, body ->
        out << "<a href=\"${createLink(controller: attrs.remove('controller'), action: attrs.remove('action'), params: attrs.remove('params'))}\" class=\"btn btn-${attrs.tipo ? attrs.remove('tipo') : 'primary'} btn-fab btn-raised fa fa-${attrs.remove('icone')}\" title=\"${attrs.remove('title')}\" "
        attrs.each { key, value ->
            out << "$key=\"$value\" "
        }
        out << '></a>'
    }

    def actionButtonFlutuante = {attrs, body ->
        attrs.class = "btn btn-${attrs.tipo ?: 'success'} btn-fab btn-raised"
        out << will.actionButton(attrs, body)
    }

    def submitButtonFlutuante = {attrs, body ->
        attrs.class = "btn btn-${attrs.tipo ?: 'success'} btn-fab btn-raised"
        out << will.submitButton(attrs, body)
    }

    def resetButtonFlutuante = {attrs, body ->
        attrs.class = "btn btn-${attrs.tipo ?: 'warning'} btn-fab btn-raised"
        out << will.resetButton(attrs, body)
    }

    private static String processarBotao(String tipo, String icone, String action, Map attrs, Closure body) {
        String out = "<button type=\"${tipo}\" name=\"_action_${action}\" "
        attrs.each { k, v ->
            out += "$k=\"${v.encodeAsHTML()}\" "
        }
        out += '>'
        if (icone) {
            out += "<i class=\"fa fa-${icone}\"></i> "
        }
        out += body()
        // close tag
        out += '</button>'
        return out
    }
}
