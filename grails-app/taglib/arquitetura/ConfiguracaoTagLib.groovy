package arquitetura

import arquitetura.configuracao.Configuracao
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException

/**
 * Created by willian.krause on 27/07/2015.
 */
class ConfiguracaoTagLib {
    static namespace = 'will'

    def config = { attrs, body ->
        String chave = attrs.remove('chave')
        if (!chave) {
            throw new GrailsTagException(g.message(code: 'default.erro.chaveObrigatoria'))
        }

        def configuracao = Configuracao.findByChave(chave)

        out << configuracao?.valor
    }
}
