package arquitetura

import arquitetura.menu.Menu
import arquitetura.menu.MenuPapel
import groovy.xml.MarkupBuilder

class MenuPrincipalTagLib {
    static namespace = 'willcrisis'

    def menuPrincipal = { attrs, body ->
        StringWriter output = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(output)
        builder.div(class: 'navbar navbar-default navbar-fixed-top hidden-print') {
            div(class: 'container-fluid top-bar-min') {
                    mkp.yieldUnescaped(
                        will.actionButton(type: 'button', icone: 'list', class: 'navbar-toggle', 'data-toggle': 'collapse', 'data-target': '.navbar-collapse')
                    )
                    a(href: createLink(uri: '/')) {
                        mkp.yieldUnescaped(asset.image(src: '/logo/logo.png', style: 'height: 64px', alt: message(code: 'sistema.nome.label')))
                    }
            }

            div(class: 'container-fluid top-bar') {
                    a(href: createLink(uri: '/')) {
                        mkp.yieldUnescaped(asset.image(src: '/logo/logo.png', style: 'height: 64px', alt: message(code: 'sistema.nome.label')))
                    }
                ul(class: 'nav navbar-nav navbar-right') {
                    mkp.yieldUnescaped(body())
                    montarMenuUsuario(ul: delegate)
                }
            }
            div(class: 'navbar-collapse collapse') {
                ul(class: 'nav navbar-nav') {
                    List<Menu> pais = Menu.findAllByPaiIsNullAndAtivo(true, [sort: 'ordem'])
                    pais.each { pai ->
                        List<Menu> filhos = Menu.findAllByPaiAndAtivo(pai, true, [sort: 'ordem'])
                        List<MenuPapel> permissoes = MenuPapel.findAllByMenu(pai)
                        sec.ifAnyGranted(roles: permissoes.collect { it.papel?.authority }.join(',')) {
                            if (filhos) {
                                montarMenuPai(ul: delegate, menu: pai, filhos: filhos)
                            } else {
                                montarMenuFilho(ul: delegate, menu: pai)
                            }
                        }
                    }
                }
                ul(class: 'nav navbar-nav navbar-right top-bar-min') {
                    montarMenuUsuario(ul: delegate)
                }

            }
        }
        out << output.toString()
    }

    private def montarMenuUsuario(attrs) {
        attrs.ul.li (class: "dropdown") {
            a(href: '#', class: "dropdown-toggle", 'data-toggle': "dropdown", sec.loggedInUserInfo(field: 'nomeCompleto').decodeHTML()) {
                b(class: 'caret') {
                    mkp.yieldUnescaped('&nbsp;')
                }
            }
            ul(class: 'dropdown-menu') {
                li {
                    a(href: createLink(controller: 'usuario', action: 'minhaConta', id: sec.loggedInUserInfo(field: 'id')), message(code: 'usuario.minhaConta.label'))
                }
                li(class: 'divider') {

                }
                li {
                    a(href: createLink(controller: 'logout'), message(code: 'sistema.logout.label'))
                }
            }
        }
    }

    private def montarMenuPai(attrs) {
        List<MenuPapel> permissoes = MenuPapel.findAllByMenu(attrs.menu)
        sec.ifAnyGranted(roles: permissoes.collect {it.papel?.authority}.join(',')) {
            attrs.ul.li(class: "dropdown${attrs.filho ? '-submenu' : ''}") {
                a(href: '#', class: "${attrs.filho ? '' : 'dropdown-toggle'}", 'data-toggle': "${attrs.filho ? '' : 'dropdown'}") {
                    span('aria-hidden': true, class: attrs.menu.icone ? "fa ${attrs.menu.icone}" : '', '')
                    t(message(code: attrs.menu.label))
                    if (!attrs.filho) {
                        b(class: 'caret') {
                            mkp.yieldUnescaped('&nbsp;')
                        }
                    }
                }
                ul(class: 'dropdown-menu') {
                    attrs.filhos.each { Menu menu ->
                        List<Menu> filhos = Menu.findAllByPaiAndAtivo(menu, true, [sort: 'ordem'])
                        if (filhos) {
                            montarMenuPai(ul: delegate, menu: menu, filhos: filhos, filho: true)
                        } else {
                            montarMenuFilho(ul: delegate, menu: menu)
                        }
                    }
                }
            }
        }
    }

    private def montarMenuFilho(attrs) {
        List<MenuPapel> permissoes = MenuPapel.findAllByMenu(attrs.menu)
        sec.ifAnyGranted(roles: permissoes.collect {it.papel?.authority}.join(',')) {
            attrs.ul.li {
                a(href: createLink(controller: attrs.menu.controlador, attrs.menu.acao)) {
                    span('aria-hidden': true, class: attrs.menu.icone ? "fa ${attrs.menu.icone}" : '', '')
                    t(message(code: attrs.menu.label))
                }
            }
        }
    }
}
