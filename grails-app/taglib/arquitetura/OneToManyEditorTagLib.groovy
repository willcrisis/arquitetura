package arquitetura

import groovy.xml.MarkupBuilder

class OneToManyEditorTagLib {
    static namespace = "will"

    def oneToMany = { attrs, body ->
        StringWriter output = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(output)
        builder.div(id: "${attrs.id}_parent") {
            div(class:"form-group") {
                div(class: 'col-lg-3') {
                    if (!attrs.readonly) {
                        mkp.yieldUnescaped(
                                will.actionButton(type: "button", onclick: "adicionar('${attrs.id}')", action: "none", '<i class="fa fa-fw fa-plus"></i>')
                        )
                    } else {
                        mkp.yieldUnescaped("&nbsp;")
                    }
                }
            }
            mkp.yieldUnescaped(
                g.render(template: "/oneToMany/oneToMany", model: [componente: attrs.componente, id: attrs.id, label: attrs.label, lista: attrs.lista, readonly: attrs.readonly], plugin: 'arquitetura')
            )
        }
        out << output.toString()
    }

    def oneToManyTable = { attrs, body ->
        StringWriter output = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(output)
        builder.table(id: "${attrs.id}_parent", class: 'table table-striped table-hover') {
            thead {
                tr {
                    mkp.yieldUnescaped(body())
                    td(class: 'acoes') {
                        mkp.yieldUnescaped(
                                will.actionButton(type: "button", onclick: "adicionarTabela('${attrs.id}')", action: "none", title: message(code: 'default.oneToMany.adicionarNoFim.label'), '<i class="fa fa-fw fa-plus"></i>')
                        )
                    }
                }
            }
            tbody {
                mkp.yieldUnescaped(
                        g.render(template: "/oneToMany/oneToManyTable", model: [componente: attrs.componente, id: attrs.id, label: attrs.label, lista: attrs.lista, readonly: attrs.readonly], plugin: 'arquitetura')
                )
            }

        }
        builder {
            mkp.yieldUnescaped(
                    will.transientPaginate(tableId: "${attrs.id}_parent")
            )
        }

        out << output.toString()
    }
}
