package base

import org.springframework.transaction.annotation.Transactional;

class BaseService {
    def grailsApplication;

    def sessionFactory

    def save(def entity) {
        if(grailsApplication.isDomainClass(entity.class)){
            entity.validate()
            if (entity.hasErrors()) {
                entity.discard()
                return null
            }
            if (entity.id && entity.version < getLastVersion(entity)) {
                entity.errors.reject('default.optimistic.locking.failure')
                entity.discard()
                return null
            } else {
                return entity.save(flush: true, failOnError: true)
            }
        }
        null;
    }

    @Transactional(readOnly = false)
    def delete(def entity){
        if(grailsApplication.isDomainClass(entity.class)){
            entity.delete(flush: true)
        }
    }

    Long getLastVersion(def entity) {
        if(grailsApplication.isDomainClass(entity.class)){
            return entity.class.withCriteria {
                eq 'id', entity.id
                order 'version', 'desc'
                projections {
                    property 'version'
                }
                uniqueResult = true
                maxResults 1
            }
        }
    }

    @Transactional(readOnly = true)
    def find(def clazz, def id) {
        clazz.findById(id);
    }

    void limparSessao() {
        sessionFactory.currentSession.flush()
        sessionFactory.currentSession.clear()
    }
}
