package arquitetura.menu

import base.BaseService

class MenuService extends BaseService {
    Menu alternarAtivacao(Long idMenu) {
        Menu menu = Menu.findById(idMenu)
        menu.ativo = !menu.ativo
        save(menu)
        menu.discard()
        return menu
    }
}
