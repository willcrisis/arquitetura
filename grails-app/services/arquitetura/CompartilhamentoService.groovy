package arquitetura

import arquitetura.seguranca.CompartilhamentoPendente
import arquitetura.seguranca.Usuario

class CompartilhamentoService {

    def mailService
    def baseService

    def grailsApplication

    def compartilhar(def registro, String email, Class classeCompartilhamento, String propriedadeObjeto, String nomeService, String caminhoViewEmail, String caminhoViewConvite, Map model = [:]) {
        model.nomeUsuario = registro.dono.nomeCompleto
        model.urlServidor = grailsApplication.config.grails.serverURL
        model.idRegistro = registro.id

        Usuario usuario = Usuario.findByEmail(email)
        if (!usuario) {
            mailService.sendMail {
                async true
                from grailsApplication.config.grails.remetente.geral
                to email
                subject 'Convite'
                body(view: caminhoViewConvite, model: model)
            }
            return baseService.save(new CompartilhamentoPendente(email: email, idRegistro: registro.id, service: nomeService, tipo: registro.class))
        }
        mailService.sendMail {
            async true
            from grailsApplication.config.grails.remetente.geral
            to usuario.email
            subject 'Convite'
            body(view: caminhoViewEmail, model: model)
        }

        def compartilhamento = classeCompartilhamento.newInstance()
        compartilhamento.usuario = usuario
        compartilhamento."${propriedadeObjeto}" = registro
        compartilhamento.dono = false
        return baseService.save(compartilhamento)
    }
}
