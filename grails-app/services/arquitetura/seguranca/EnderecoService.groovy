package arquitetura.seguranca

import arquitetura.command.EnderecoCommand
import base.BaseService
import org.codehaus.groovy.grails.web.binding.DataBindingUtils

class EnderecoService extends BaseService {

    def springSecurityService

    void salvarEndereco(EnderecoCommand command) {
        if (!validarCamposObrigatorios(command)) {
            return
        }

        Endereco endereco
        if (command.id) {
            endereco = Endereco.findById(command.id)
            endereco.version = command.versao
        } else {
            endereco = new Endereco()
        }
        endereco.url = command.url
        endereco.configAttribute = command.configAttribute.join(',')

        save(endereco)
        if (endereco.hasErrors()) {
            return
        }
        springSecurityService.clearCachedRequestmaps()
    }

    private boolean validarCamposObrigatorios(EnderecoCommand command) {
        command.validate()
        return !command.hasErrors()
    }

    EnderecoCommand criarCommand(Long id) {
        Endereco endereco = Endereco.findById(id)
        EnderecoCommand command = new EnderecoCommand()
        DataBindingUtils.bindObjectToInstance(command, endereco.properties, null, ['configAttribute', 'httpMethod'], null)
        command.id = endereco.id
        command.versao = endereco.version
        command.configAttribute = endereco.configAttribute?.split(',')
        return command
    }
}
