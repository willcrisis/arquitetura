package arquitetura.seguranca

import arquitetura.command.UsuarioCommand
import base.BaseService
import org.codehaus.groovy.grails.web.binding.DataBindingUtils
import org.springframework.context.ApplicationContext
import org.springframework.transaction.annotation.Transactional
import utils.TratadorDeParametros

class UsuarioService extends BaseService {
    def springSecurityService
    def mensagemService
    def mailService

    void salvarUsuario(UsuarioCommand command) {
        if (!validarCamposObrigatorios(command)) {
            return
        }

        Usuario usuario
        if (command.id) {
            usuario = Usuario.findById(command.id)
            usuario.version = command.versao
        } else {
            usuario = new Usuario()
        }
        if (command.alteracao) {
            DataBindingUtils.bindObjectToInstance(usuario, command.properties, null, ['password', 'alteracao', 'permissoes', 'class'], null)
            if (command.password) {
                usuario.password = command.password
            }
        } else {
            DataBindingUtils.bindObjectToInstance(usuario, command.properties, null, ['alteracao', 'permissoes', 'class'], null)
        }

        save(usuario)
        if (usuario.hasErrors()) {
            usuario.errors.allErrors.each {
                command.errors.reject(it.code, it.arguments, 'mensagem padrao')
            }
            return
        }
        salvarPermissoes(command, usuario)
    }

    void salvarPermissoes(UsuarioCommand command, Usuario usuario) {
        command.clearErrors()
        if (!validarListaPermissoes(command)) {
            return
        }
        Permissao.removeAll(usuario)
        command.permissoes.each {
            save(new Permissao(usuario: usuario, papel: it))
        }
    }

    void alterarSenha(UsuarioCommand command) {
        command.clearErrors()
        Usuario usuario = Usuario.findById(command.id)
        if (!validarSenhas(command, usuario)) {
            return
        }
        usuario.version = command.versao
        usuario.password = command.novaSenha
        save(usuario)
        springSecurityService.reauthenticate(usuario.username, usuario.password)
    }

    void registrar(UsuarioCommand command, OAuthID oAuthID) {
        command.clearErrors()
        Usuario usuario = new Usuario()
        DataBindingUtils.bindObjectToInstance(usuario, command.properties, null, ['password', 'alteracao', 'permissoes', 'class'], null)
        usuario.username = usuario.email
        if (!validarIgualdadeSenhas(command)) {
            return
        }
        usuario.password = command.novaSenha
        if (oAuthID) {
            oAuthID.user = usuario
            usuario.addTooAuthIDs(oAuthID)
        }
        save(usuario)
        if (usuario.hasErrors()) {
            usuario.errors.allErrors.each {
                command.errors.reject(it.code, it.arguments, 'mensagem padrao')
            }
            return
        }
        save(new Permissao(usuario: usuario, papel: Papel.findByAuthority('ROLE_USER')))
        mailService.sendMail {
            async true
            multipart true
            from grailsApplication.config.grails.remetente.cadastro
            to usuario.email
            subject 'Novo Cadastro'
            html(view: '/usuario/mail/registro', model: [nome: usuario.nomeCompleto, urlServidor: grailsApplication.config.grails.serverURL])
//            inline 'logo', 'image/jpg', new File('./assets/images/logo/logo-64.png')
        }
        log.info(new File('.').absolutePath)
        log.info('Novo usuario cadastrado: ' + usuario.nomeCompleto)
        List<CompartilhamentoPendente> compartilhamentos = CompartilhamentoPendente.findAllByEmail(usuario.email)
        ApplicationContext ctx = grailsApplication.mainContext
        compartilhamentos.each {
            def registro = (it.tipo).findById(it.idRegistro)
            def service = ctx.getBean(it.service)
            service.compartilhar(registro, it.email)
            it.delete(flush: true)
        }
    }

    private boolean validarSenhas(UsuarioCommand command, Usuario usuario) {
        if (!springSecurityService.passwordEncoder.isPasswordValid(usuario.password, command.password, null)) {
            command.errors.reject('usuario.senhaIncorreta.message')
        }
        return validarIgualdadeSenhas(command)
    }

    private boolean validarIgualdadeSenhas(UsuarioCommand command) {
        if (command.novaSenha != command.confirmacaoSenha) {
            command.errors.reject('usuario.confirmacaoIncorreta.message', [mensagemService.traduzir('usuario.novaSenha.label'), mensagemService.traduzir('usuario.confirmacaoSenha.label')] as Object[], 'Campos invalidos: {0} e {1}')
        }
        return !command.hasErrors()
    }

    private static boolean validarCamposObrigatorios(UsuarioCommand command) {
        command.validate()
        return !command.hasErrors()
    }

    private static boolean validarListaPermissoes(UsuarioCommand command) {
        if (!command.permissoes || command.permissoes.size() < 1) {
            command.errors.rejectValue('permissoes', 'default.invalid.min.size.message')
        }
        return !command.hasErrors()
    }

    UsuarioCommand criarCommand(Long id) {
        Usuario usuario = Usuario.findById(id)
        UsuarioCommand command = new UsuarioCommand()
        DataBindingUtils.bindObjectToInstance(command, usuario.properties)
        command.id = usuario.id
        command.versao = usuario.version
        List<Permissao> permissoes = Permissao.findAllByUsuario(usuario)
        command.permissoes = permissoes.papel
        return command
    }

    Usuario alternarCampo(Long id, String campo, String version) {
        Usuario usuario = Usuario.findById(id)
        usuario.version = TratadorDeParametros.somenteNumerosLong(version)
        usuario."${campo}" = !usuario."${campo}"
        save(usuario)
        return usuario
    }

    @Transactional(readOnly = false)
    def delete(def entidade) {
        Permissao.removeAll(entidade)
        super.delete(entidade)
    }
}
