package arquitetura.bug

import base.BaseService

class BugService extends BaseService {

    def springSecurityService
    def mensagemService

    def incluir(Bug bug) {
        bug.data = new Date()
        bug.usuario = springSecurityService.currentUser

        save(bug)
        if (bug.hasErrors()) {
            return bug
        }

        save(new HistoricoBug(situacao: SituacaoBug.emAndamento.get(), bug: bug, data: new Date(), observacoes: mensagemService.traduzir('historicoBug.observacaoInicial.label')))

        return bug
    }

    def alterar(Bug bug, Long idNovaSituacao, String observacoes) {
        save(bug)
        if (bug.hasErrors()) {
            return null
        }

        save(new HistoricoBug(bug: bug, situacao: SituacaoBug.findById(idNovaSituacao), data: new Date(), observacoes: observacoes))
        return bug
    }
}
