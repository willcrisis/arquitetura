package arquitetura.i18n

import base.BaseService

class I18nService extends BaseService {
    I18n consultar(String chave, Locale idioma) {
        I18n.findByKeyAndLanguage(chave, idioma)
    }
}
