package arquitetura.i18n

import org.springframework.context.i18n.LocaleContextHolder

class MensagemService {
    def messageSource

    String traduzir(String chave, String... argumentos) {
        return traduzir(chave, LocaleContextHolder.getLocale(), argumentos)
    }

    String traduzir(String chave, Locale idioma, String... argumentos) {
        messageSource.getMessage(chave, argumentos as Object[], chave, idioma)
    }
}
