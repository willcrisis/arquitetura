println '''
Plugin de arquitetura instalado!! Execute 'grails arq-install' para configurar a aplicação, mas cuidado: essa ação é irreversível.
É recomendável usar esse plugin em um projeto criado do zero. Caso seu projeto já esteja previamente implementado, não
deixe de consultar a documentação disponível em:
${URL}
'''