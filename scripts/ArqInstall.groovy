import org.codehaus.groovy.grails.cli.CommandLineHelper

includeTargets << grailsScript("_GrailsInit")

USAGE = """
arq-install
Instala os recursos e configura a aplicação para uso do plugin. Essa ação é irreversível.
"""

appDir = "$basedir/grails-app"

target(main: "Instala os recursos e configura a aplicação para uso do plugin. Essa ação é irreversível.") {
    def inputHelper = new CommandLineHelper()
    String input = inputHelper.userInput('Tem certeza de que deseja executar as configurações? Essa ação é irreversível. Digite \'SIM\' (sem aspas) para confirmar:')
    if (input.toUpperCase() == 'SIM') {
        copiarTemplates()
        removerMainGsp()
        escreverConfiguracaoSpring()
        File i18n = removerArquivosInternacionalizacao()
        escreverArquivoInternacionalizacao(i18n)
        removerAssets()
        adicionarReferenciasAssets()
        escreverConfiguracoes()
        println 'Projeto configurado com sucesso. Se você usa alguma IDE, certifique-se de sincronizar o projeto para ver as mudanças.'
    } else {
        println 'A configuração do projeto não foi efetuada.'
    }

}

private void escreverConfiguracoes() {
    File config = new File(appDir, 'conf/Config.groovy')
    println 'Escrevendo configurações de binders...'
    config.append("\ngrails.databinding.dateFormats = ['dd/MM/yyyy']")
    println 'Escrevendo configurações de envio de e-mail...'
    config.append('''
grails {
       mail {
              host = "smtp.gmail.com"
              port = 465
              username = "krause.willian@gmail.com"
              password = "kvfxumvcevnomczg"
              props = ["mail.smtp.auth":"true",
                       "mail.smtp.socketFactory.port":"465",
                       "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
                       "mail.smtp.socketFactory.fallback":"false"]
       }
}
''')
}

private void removerAssets() {
    println 'Removendo assets desnecessários...'
    for (String url in ['errors', 'main', 'mobile']) {
        File stylesheet = new File(appDir, "assets/stylesheets/${url}.css")
        stylesheet.delete()
    }
}

private void adicionarReferenciasAssets() {
    println 'Escrevendo assets...'
    File javascript = new File(appDir, 'assets/javascripts/application.js')
    javascript.createNewFile()
    javascript.write('''//= require_self
//= require arquitetura
''', 'UTF-8')

    File stylesheet = new File(appDir, 'assets/stylesheets/application.css')
    stylesheet.createNewFile()
    stylesheet.write('''/*
*= require arquitetura
*= require_self
*/
''', 'UTF-8')
}

private File removerArquivosInternacionalizacao() {
    println 'Removendo arquivos de internacionalização desnecessários...'
    File i18n = new File(appDir, 'i18n')
    i18n.eachFile {
        it.delete()
    }
    i18n
}

private void escreverArquivoInternacionalizacao(File i18n) {
    println 'Escrevendo arquivos de internacionalização...'
    File messages = new File(i18n, 'messages.properties')
    messages.createNewFile()
    messages.write("sistema.nome.label=Willcrisis.com - ${metadata.'app.name'}")
    messages.append("\nsistema.versao=0.1.0")
    File messagesPt = new File(i18n, 'messages_pt_BR.properties')
    messagesPt.createNewFile()
    messagesPt.write("sistema.nome.label=Willcrisis.com - ${metadata.'app.name'}")
}

private void escreverConfiguracaoSpring() {
    println 'Escrevendo arquivo resources.groovy...'
    File resources = new File(appDir, 'conf/spring/resources.groovy')
    resources.createNewFile()
    resources.write('''import arquitetura.seguranca.UsuarioUserDetailsService

beans = {
    userDetailsService(UsuarioUserDetailsService)
}''', 'UTF-8')
}

private void removerMainGsp() {
    println 'Removendo main.gsp...'
    new File(appDir, 'views/layouts/main.gsp').delete()
}

private void copiarTemplates() {
    println 'Criando pasta de templates...'
    ant.mkdir(dir: "${basedir}/src/templates")
    println 'Copiando arquivos de template...'
    ant.copy(todir: "$basedir/src/templates") {
        fileset(dir: "$arquiteturaPluginDir/src/templates")
    }
}

setDefaultTarget(main)
